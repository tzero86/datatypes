/*
    Pirple's ES6 Keeping up with the JavaScripts course
    Project #1 a simple todo-list management app
    By tzero86
*/

// Some options for the toaster messages that replaced the alerts
toastr.options.timeOut = 1200;
toastr.options.closeButton = true;
toastr.options.positionClass = "toast-top-center";


// we set the app data
const appMetaData = {
    "name": "WriteCafe",
    "tagLine": "A simple to-do list management app.",
    "version": "0.1.0",
    "author": "tzero86",
    "modalTexts": {
        addNewNote: {
            name: "Add New Note",
            helpText: "Here you can create and save notes. Note Name is required to save the note."
        },
        editNote: {
            name: "Edit Note",
            helpText: "Here you can edit and save your note. Note Name cannot be empty."
        },
        accountSettings: {
            name: "Account Settings",
            helpText: "Here you can edit and save your email, userName and Password."
        },
        dashboardHelp: {
            name: "User Dashboard",
            helpText: `You can create new notebooks by clicking <i class="la la-plus-circle">Add</i> button. <br> To view and edit an existing notebook click on the <i class="la la-pencil" style="font-size: 20px" ></i> icon or the Notebook Name.
            To remove a notebook click on the <i class="la la-trash" style="font-size: 20px"></i> icon. <br>To remove all existing notes click on the <i class="la la-exclamation-triangle">Wipe</i> button. <br>`
        },
        registerForm: {
            name: "",
            helpText: `Enter your email, userName and Password to register.`
        }
    }
};

// we update the page title
document.title = `${appMetaData.name}`;

// we define the templates that will be appended to the body
const appTemplate = `
    <h1>${appMetaData.name}</h1>
    <p>${appMetaData.tagLine}</p>
    <form>
        <button id="login" class="login">Log In</button>
        <button id="signup" class="signup">Sign Up</button>
    </form>
    <footer class="footer"><p>${appMetaData.name} ©2019 by ${appMetaData.author}</p></footer>`;

const appendAppContainer = () => { // the function that appends the appContainer template to the body
    const appContainer = document.createElement("div");
    appContainer.innerHTML = `${appTemplate}`;
    appContainer.classList.add("container");
    appContainer.classList.add("content");
    appContainer.setAttribute("id", "app");
    document.body.appendChild(appContainer);
    setupHomeEvents();
}

const editNoteTemplate = `
    <div class="modal-content noteLeft">
        <span class="close-button"><i class="la la-times-circle-o close-button" style="font-weight:bold; font-size:15px"></i></span>
        <form class="settingsFix">
            <h2>${appMetaData.modalTexts.editNote.name}</h2>
            <p style="font-size: 12px; margin-top: 10px; margin-bottom: 20px; text-align: center;">${appMetaData.modalTexts.editNote.helpText}</p>
            <label id="noteID" hidden></label>
            <label for="editedNoteBookName">Note Name:</label>
            <input id="editedNoteBookName" type="text" placeholder="Enter a Note Name."/>
            <hr>
            <div id="noteList">
                <ul id="notes">
                </ul>
            </div>
            <p id="notesCounter">1 Note(s)</p>
        </form>
        <button id="saveEditedNote" class="login settingsFix" style="margin: 10px;">Save</button>
    </div>`;

const appendEditNoteModal = () => { // the function that appends the editNoteModal template to the body
    const editModal = document.createElement("div");
    editModal.innerHTML = `${editNoteTemplate}`;
    editModal.classList.add("modal");
    editModal.classList.add("editnoteModal");
    editModal.setAttribute("id", "editNote");
    editModal.setAttribute("style", "z-index:99");
    document.body.appendChild(editModal);
}

const addNoteTemplate = `
    <div class="modal-content noteLeft">
        <span class="close-button"><i class="la la-times-circle-o close-button" style="font-weight:bold; font-size:15px"></i></span>
            <form class="settingsFix">
                <h2>${appMetaData.modalTexts.addNewNote.name}</h2>
                <p style="font-size: 12px; margin-top: 10px; margin-bottom: 20px; text-align: center;">${appMetaData.modalTexts.addNewNote.helpText}</p>
                <label for="noteBookName">Note Name:</label>
                <input id="noteBookName" type="text" placeholder="Enter a Note Name."/>
                <hr>
                <div id="noteList">
                    <ul id="notes">
                    </ul>
                </div>
                <p id="notesCounter">1 Note(s)</p>
            </form>
            <button id="saveNewNote" class="login settingsFix" style="margin: 10px;">Save</button>
    </div>`;

const appendAddNoteModal = () => { // the function that appends the addNewNoteModal template to the body
    const addNoteModal = document.createElement("div");
    addNoteModal.innerHTML = `${addNoteTemplate}`;
    addNoteModal.classList.add("modal");
    addNoteModal.classList.add("notesModal");
    addNoteModal.setAttribute("id", "addNewNote");
    addNoteModal.setAttribute("style", "z-index:99;");
    document.body.appendChild(addNoteModal);
}

const accountSettingsTemplate = `
    <div class="modal-content">
        <span class="close-button"><i class="la la-times-circle-o close-button" style="font-weight:bold; font-size:15px"></i></span>
        <form class="settingsFix">
            <h2>${appMetaData.modalTexts.accountSettings.name}</h2>
            <p style="font-size: 12px; margin-top: 10px; margin-bottom: 20px; text-align: center;">${appMetaData.modalTexts.accountSettings.helpText}</p>
            <input id="storedUserName" type="text" placeholder="username" /><input id="storedUserEmail" type="email" placeholder="your@email.com"/>
            <input placeholder="password" id="storedUserPassword" type="password"/>
        </form>
        <button id="saveSettings" class="login settingsFix" style="margin: 10px;">Save</button>
    </div>`;

const appendSettingsModal = () => { // the function that appends the account settings template to the body
    const accSettingsModal = document.createElement("div");
    accSettingsModal.innerHTML = `${accountSettingsTemplate}`;
    accSettingsModal.classList.add("modal");
    accSettingsModal.setAttribute("id", "settingsModal");
    accSettingsModal.setAttribute("style", "z-index:99;");
    document.body.appendChild(accSettingsModal);
}

const dashboardTemplate = `
<div id="userDashboard" class="userDashboard" >
    <h4 class="goLeft">${appMetaData.modalTexts.dashboardHelp.name}</h4>
    <div id="dashboardMenu">
        <div class="divTable">
            <div class="divTableBody">
                <div class="divTableRow">
                <div class="divTableCell fix"><i class="la la-info-circle" style="font-size: 35px; font-weight:bold;"></i></div>
                <div class="divTableCell">
                    <p class="fix">${appMetaData.modalTexts.dashboardHelp.helpText}</p>
                </div>
            </div>
        </div>
    </div>
        <i id="addNewNote" class="la la-plus-circle">Add</i>
        <i id="deleteAllNotes" class="la la-exclamation-triangle">Wipe</i>
    </div>
    <div id="dashboarUserNotes">
        <section>
            <div class="tbl-header">
                <table cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <th>NoteBook Name</th>
                            <th>Date & Time Created</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        <div class="tbl-content">
            <table cellpadding="0" cellspacing="0" border="0">
                <tbody>
                </tbody>
        </table>
            </div>
    </section>
    </div>
</div>
</div>`;

// Here he append the templates to the body and clean up any old sessions
window.addEventListener("load", function (e) {
    if (localStorage["ActiveSession"]) {
        localStorage.removeItem("ActiveSession");
    }
    appendAppContainer();
    appendSettingsModal();
    appendEditNoteModal();
    appendAddNoteModal();
});

// we grab the homePage elements
const setupHomeEvents = () => {
    setTimeout(() => {
        var appDiv = document.getElementById("app");
        var homeLogin = appDiv.querySelector("#login");
        var homeSignUp = appDiv.querySelector("#signup");

        // handles user login form.
        const userLogin = () => {
            homeLogin.removeEventListener("click", userLogin);
            updatePage("login");
        };

        // global events for the home page buttons.
        homeLogin.addEventListener("click", function (event) {
            event.preventDefault();
            userLogin();
        });

        homeSignUp.addEventListener("click", function (event) {
            event.preventDefault();
            updatePage("register");
        });
    }, 600);
}

// we create a new note-item element and append it to the modal, we also setup all new-item's events
const addNewNoteItem = (parentModal) => {
    let modal = null;
    let updating = false;
    if (typeof parentModal === 'undefined') {
        modal = document.querySelector(".notesModal");
    } else {
        modal = parentModal;
        updating = true;
    }

    const listOfItems = modal.querySelector("#notes");
    const notesCounter = modal.querySelector("#notesCounter");
    const newItem = document.createElement("li");
    const itemName = document.createElement("input");
    const itemAddButton = document.createElement("button");
    const itemDelButton = document.createElement("button");
    const itemDoneButton = document.createElement("button");

    // we set the required classes, attributes for the elements
    itemName.classList.add("note");
    itemName.setAttribute("type", "text");
    itemName.setAttribute("placeholder", "Item Name");
    itemAddButton.innerHTML = "Add";
    itemAddButton.classList.add("enjoy-css");
    itemDelButton.innerHTML = "Del";
    itemDelButton.classList.add("enjoy-css");
    itemDoneButton.innerHTML = "Done";
    itemDoneButton.classList.add("enjoy-css");

    // We append the elements of a note to the LI
    newItem.appendChild(itemName);
    newItem.appendChild(itemAddButton);
    newItem.appendChild(itemDelButton);
    newItem.appendChild(itemDoneButton);

    // we append the LI to the UL
    listOfItems.appendChild(newItem);
    notesCounter.innerText = `${listOfItems.querySelectorAll('li').length} note item(s).`;

    // we set up the events
    itemDoneButton.addEventListener("click", function (e) {
        e.preventDefault();
        let item = e.target;
        let ul = item.parentElement;
        if (item.innerText === "Reset") {
            item.innerText = "Done";
        } else {
            item.innerText = "Reset"
        };
        let inputBox = ul.querySelector("input");
        inputBox.classList.toggle("strike");
    });

    itemDelButton.addEventListener("click", function (e) {
        e.preventDefault();
        let item = e.target;
        let listItem = item.parentElement;
        let list = listItem.parentElement;
        if (listOfItems.querySelectorAll('li').length > 1) {
            list.removeChild(listItem);
            notesCounter.innerText = `${listOfItems.querySelectorAll('li').length} note item(s).`;
        }
    });

    itemAddButton.addEventListener("click", function (e) {
        e.preventDefault();
        if (updating) {
            addNewNoteItem(modal);
        } else {
            addNewNoteItem();
        }
    });

};

// A helper function to generate the time stamp for the notes created.
const getCurrentDate = () => {
    let today = new Date();
    let stamp = today.toLocaleTimeString();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = mm + '/' + dd + '/' + yyyy;
    return `${today} - ${stamp}`;
}

// A helper function to avoid having notebooks with duplicated names
const isNoteNameInUse = (noteName) => {
    let user = getActiveUser();
    if (user.notes.some(note => note.notebookName === noteName)) {
        return true
    } else {
        return false
    }
};

// This function reads the modal input values and stores them into the usersData object.
const saveNewNote = () => {
    const modal = document.querySelector(".notesModal");
    const ulElement = modal.querySelector("#notes");
    const listOfLI = modal.querySelectorAll("#notes li input");
    const noteNameInput = modal.querySelector("#noteBookName");
    if (noteNameInput.value !== "") { // && !isNoteNameInUse(noteNameInput.value)) {
        const activeUser = getActiveUser()
        if (activeUser) {
            let newItem = {
                id: `Note-${uuidv4().slice(-8)}`,
                notebookName: noteNameInput.value,
                items: [],
                dateCreated: getCurrentDate()
            }
            for (let index = 0; index < listOfLI.length; index++) {
                const item = listOfLI[index];
                const newUserItem = {
                    itemName: "",
                    isCompleted: false
                }
                newUserItem.itemName = item.value;
                newUserItem.isCompleted = item.classList.contains("strike");
                newItem.items.push(newUserItem);
            }
            activeUser.notes.push(newItem);
            const stringy = JSON.stringify(activeUser);
            localStorage.setItem(getActiveUser(true), stringy);
            toastr.success("Saved Successfully!");
            modal.classList.toggle("show-modal");
            clearNotes();
            loadUserNotes();
            return;
        }
        toastr.error("No Active user Found!");
        modal.classList.toggle("show-modal");
        return;
    }
    toastr.error("Note Name cannot be empty!");
    return;
};

// Draws the newNote form
const drawNewNoteForm = () => {
    const modal = document.querySelector(".notesModal");
    const ulElement = modal.querySelector("#notes");
    const listOfItems = modal.querySelectorAll("#notes li");
    const saveNoteButton = modal.querySelector("#saveNewNote");
    // we reset the form
    listOfItems.forEach(item => {
        ulElement.removeChild(item);
    });
    saveNoteButton.addEventListener("click", saveNewNote)
    addNewNoteItem();
}

// Toggles the add new note modal
const toggleAddNewNote = () => {
    const modal = document.querySelector(".notesModal");
    modal.classList.toggle("show-modal");
    drawNewNoteForm();
};

// Setup the events for the add note modal.
const setupNotesModalEvents = () => {
    const modal = document.querySelector(".notesModal");
    const closeButton = modal.querySelector(".close-button");
    closeButton.addEventListener("click", toggleAddNewNote);
};

// We setup the edit note modal events.
const setupEditNoteModalEvents = () => {
    const modal = document.querySelector(".editnoteModal");
    const closeButton = modal.querySelector(".close-button");
    closeButton.addEventListener("click", toggleEditNoteModal);
};

// We invoke the add new note modal.
const addNewNote = () => {
    toggleAddNewNote();
};

// main function to toggle the account settings modal
const toggleModal = () => {
    const modal = document.querySelector("#settingsModal");
    modal.classList.toggle("show-modal");
};

// Sets the required events for the accountSettings modal
const setupModalEvents = () => {
    setTimeout(() => {
        let modal = document.querySelector("#settingsModal");
        let closeButton = modal.querySelector(".close-button");
        let logOutContainer = document.querySelector("#logOutContainer")
        let settingsButton = logOutContainer.querySelector(".accountSettings");
        let saveButton = modal.querySelector("#saveSettings")
        const user = getActiveUser();
        modal.style.zIndex = 99;
        closeButton.addEventListener("click", toggleModal);
        settingsButton.addEventListener("click", toggleModal);
        saveButton.addEventListener("click", function (e) {
            e.preventDefault();
            updateUserData(user);
        });
    }, 1500);
};

// retrieves a given userID from the Local Storage
const getUserFromStorage = userID => {
    let userData = JSON.parse(localStorage.getItem(userID));
    return userData;
};

// Here we update the user details at the account settings page.
const updateUserData = (user) => {
    const userName = document.querySelector("#storedUserName");
    const userEmail = document.querySelector("#storedUserEmail");
    const password = document.querySelector("#storedUserPassword");
    let changesMade = false;
    if (userName.value !== user.usrName) {
        user.usrName = userName.value;
        changesMade = true;
    }
    if (user.usrEmail !== userEmail.value) {
        user.usrEmail = userEmail.value;
        changesMade = true;
    }
    if (vernam(password.value, user.usrID) !== user.usrPassword) {
        user.usrPassword = vernam(password.value, user.usrID);
        changesMade = true;
    }
    if (changesMade) {
        localStorage.setItem(user.usrID, JSON.stringify(user));
        console.log(`User Updated: ${user.usrID} - ${JSON.stringify(user)}`);
        toastr.success("All changes were saved successfully.");
        toggleModal();
    } else {
        toastr.warning("No changes were made.", "Nothing Saved");
        toggleModal();
    }

};

// we create and append the account settings modal.
const loadSettingsModalData = () => {
    toggleModal();
    // we populate the forms fields with the stored user data
    const modal = document.querySelector("#settingsModal");
    const userName = modal.querySelector("#storedUserName");
    const userEmail = modal.querySelector("#storedUserEmail");
    const password = modal.querySelector("#storedUserPassword");
    const user = getActiveUser();
    userName.value = user.usrName;
    userEmail.value = user.usrEmail;
    password.value = vernam(user.usrPassword, user.usrID);

};

// a simple vernam-style cipher for the passwords
// credit: http://cwestblog.com/2013/08/19/javascript-simple-vernam-cipher/
const vernam = (msg, key) => {
    let l = key.length;
    let fromCharCode = String.fromCharCode;
    return msg.replace(/[\s\S]/g, function (c, i) {
        return fromCharCode(key.charCodeAt(i % l) ^ c.charCodeAt(0));
    });
};

// checks if the user is a valid registered user.
const isValidUser = userObject => {
    for (var i = 0; i < localStorage.length; i++) {
        let itemName = localStorage.key(i);
        if (itemName.startsWith("UserID-")) {
            let user = JSON.parse(localStorage.getItem(itemName));
            if (user.usrName === userObject.usrName) {
                let decryptedPass = vernam(user.usrPassword, user.usrID);
                if (decryptedPass === userObject.usrPassword) {
                    localStorage.setItem("ActiveSession", itemName);
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
    return false;
};

// The wipe all notes feature.
const deleteAllUserNotes = () => {
    let user = getActiveUser();
    let notesDeleted = false;
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover the notebooks!",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            user.notes = [];
            notesDeleted = true;
            localStorage.setItem(user.usrID, JSON.stringify(user));
            clearNotes();
            loadUserNotes();
            toastr.success("All user notes were deleted.")
            return notesDeleted;
        } else {
            toastr.info("No user notes were deleted.")
            return notesDeleted;
        }
    });
};

// Draws the dashboard UI elements, used diff approach here using InnerHTML and backtics.
const drawDashboard = () => {
    const appDiv = document.getElementById("app");
    const h1 = document.createElement("h1");
    const dashDiv = document.createElement("div");
    dashDiv.setAttribute("id", "dashboard");
    h1.innerHTML = `${appMetaData.name}`;
    h1.style.textAlign = "left";
    appDiv.appendChild(h1);
    appDiv.appendChild(dashDiv);
    appDiv.classList.toggle("container");
    appDiv.classList.toggle("app");
    dashDiv.innerHTML = `${dashboardTemplate}`;
    const addNotebutton = document.getElementById("addNewNote");
    const wipeAllNotesbutton = document.getElementById("deleteAllNotes");
    setTimeout(() => {
        addNotebutton.addEventListener("click", function (e) {
            e.preventDefault();
            toggleAddNewNote();
        });

        wipeAllNotesbutton.addEventListener("click", function (e) {
            e.preventDefault();
            deleteAllUserNotes();
        })
    }, 1000);
    drawLogOutMenu();
};

// shows the error about invalid user/password combination.
const showLoginError = () => {
    toastr.error("Incorrect username or parssword.")

};

// Logs the user.
const logUser = () => {
    const user = document.getElementById("userName");
    const pass = document.getElementById("userPassword");
    let username = user.value;
    let password = pass.value;
    if (username !== "" && password !== "") {
        let user = {
            usrName: username,
            usrPassword: password,
            usrEmail: ""
        };
        if (isValidUser(user)) {
            updatePage("dash");
            toastr.success(`Welcome back ${user.usrName}!`);
            return;
        } else {
            showLoginError();
        }
    } else {
        showLoginError();
    }
};

// A little helper function to update the UI
const userRegistration = () => {
    updatePage("register");
};

// handles user login form.
const userLogin = () => {
    var appDiv = document.getElementById("app");
    var homeLogin = appDiv.querySelector("#login");
    homeLogin.removeEventListener("click", userLogin);
    updatePage("login");
};

// Takes care of drawing the home page elements, using innerHTML and backtics to define multiline HTML.
const drawHomePage = () => {
    var appDiv = document.getElementById("app");
    appDiv.classList.add("container");
    appDiv.innerHTML = `${appTemplate}`;
    const homeLogin = document.getElementById("login");
    const signUp = document.getElementById("signup");
    homeLogin.addEventListener("click", function (event) {
        event.preventDefault();
        userLogin();
    });
    signUp.addEventListener("click", function (event) {
        event.preventDefault();
        userRegistration();
    });
    return;
};

// Draws and appends the footer.
const drawFooter = () => {
    const appDiv = document.getElementById("app");
    const foot = document.createElement("footer");
    foot.classList.add("footer");
    foot.innerHTML = `<p>${appMetaData.name} ©2019 by ${appMetaData.author}</p>`;
    appDiv.appendChild(foot);
};

// credit: https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
const uuidv4 = () => {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
        var r = (Math.random() * 16) | 0,
            v = c == "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16).split();
    });
};

// we register a new user and save it to the local storage
const addNewUser = () => {
    const userEmail = document.getElementById("userRegisEmail");
    const userPassword = document.getElementById("userRegisPassword");
    const userName = document.getElementById("userRegisName");
    if (
        userEmail.value !== "" &&
        userName.value !== "" &&
        userPassword.value !== ""
    ) {
        const newUser = {
            usrID: `UserID-${uuidv4().slice(-8)}`,
            usrEmail: userEmail.value,
            usrName: userName.value,
            usrPassword: userPassword.value,
            notes: []
        };
        let newPass = vernam(newUser.usrPassword, newUser.usrID);
        newUser.usrPassword = newPass;
        let stringy = JSON.stringify(newUser);
        localStorage.setItem(`${newUser.usrID}`, stringy);
        userEmail.value = "";
        userEmail.value = "";
        userName.value = "";
        const session = {
            id: ""
        };
        session.id = newUser.usrID;
        localStorage.setItem("ActiveSession", JSON.stringify(session.id));
        updatePage("dash");
        toastr.success("You've successfully registered.", `Welcome to ${appMetaData.name}`)
    } else {
        toastr.error("username, email and password can't be empty.");
    }
};

// We create  the registration form and show it
const drawRegistration = () => {
    const appDiv = document.getElementById("app");
    const registForm = document.createElement("form");
    const h1 = document.createElement("h1");
    const description = document.createElement("p");
    const userName = document.createElement("input");
    const userEmail = document.createElement("input");
    const password = document.createElement("input");
    const registerButton = document.createElement("button");
    const registerCancelButton = document.createElement("button");
    const termsCheckbox = document.createElement("input");
    const termsLabel = document.createElement("label");
    const tersmContainer = document.createElement("div");

    // we set attributes, classes
    tersmContainer.classList.add("termsContainer");
    termsCheckbox.setAttribute("type", "checkbox");
    termsCheckbox.setAttribute("id", "termsBox");
    termsLabel.innerText = "I agree with the terms & conditions."
    termsLabel.setAttribute("for", "termsBox");
    termsLabel.classList.add("settingsFix");
    h1.innerHTML = `${appMetaData.name}`;
    description.innerHTML = `${appMetaData.modalTexts.registerForm.helpText}`;
    description.style.fontSize = "12px";
    description.style.margin = "10px";
    registerButton.innerText = "Register";
    registerButton.classList.add("login");
    userName.setAttribute("id", "userRegisName");
    userName.setAttribute("type", "username");
    userName.setAttribute("placeholder", "username");
    userEmail.setAttribute("id", "userRegisEmail");
    userEmail.setAttribute("type", "email");
    userEmail.setAttribute("placeholder", "your@email.com");
    password.setAttribute("placeholder", "password");
    password.setAttribute("id", "userRegisPassword");
    password.setAttribute("type", "password");
    registerCancelButton.innerText = "Cancel";
    registerCancelButton.classList.add("cancel");
    description.style.textAlign = "center";

    // we append the childs to the parents.
    registForm.appendChild(h1);
    registForm.appendChild(description);
    registForm.appendChild(userName);
    registForm.appendChild(userEmail);
    registForm.appendChild(password);
    tersmContainer.appendChild(termsCheckbox);
    tersmContainer.appendChild(termsLabel);
    registForm.appendChild(tersmContainer);
    registForm.appendChild(registerButton);
    registForm.appendChild(registerCancelButton);
    appDiv.appendChild(registForm);

    // we set the events
    registerButton.addEventListener("click", function (event) {
        event.preventDefault();
        if (document.querySelector('#termsBox').checked) {
            addNewUser();
        } else {
            toastr.error("You must accept the terms to register.");
        }
    });
    registerCancelButton.addEventListener("click", (e) => {
        e.preventDefault();
        homePage();
    });
};

// A function to log the user out
const logUserOut = e => {
    localStorage.removeItem("ActiveSession");
    const logOut = document.getElementById("logOutLink");
    logOut.removeEventListener("click", function (e) {});
    const container = document.getElementById("logOutContainer");
    container.parentNode.removeChild(container);
    toastr.info("User Session ended.");
    updatePage("home");
};

// A function to create the top right corner menu with the Log Out and Account settings options.
const drawLogOutMenu = () => {
    var appDiv = document.getElementById("app");
    const user = getActiveUser();
    const container = document.createElement("div");
    const logOutLink = document.createElement("a");
    const accountSettings = document.createElement("a");
    container.setAttribute("id", "logOutContainer");
    logOutLink.setAttribute("id", "logOutLink");
    accountSettings.setAttribute("id", "accountSettings");
    accountSettings.classList.add("accountSettings");
    accountSettings.innerText = "Account Settings";
    logOutLink.innerText = `Log Out(${user.usrName})`;
    container.appendChild(logOutLink);
    container.appendChild(accountSettings);
    logOutLink.setAttribute("href", "#");
    accountSettings.setAttribute("href", "#");
    accountSettings.classList.add("trigger");
    logOutLink.classList.add("logout");
    container.classList.add("logoutContainer");
    document.body.insertBefore(container, appDiv);
    logOutLink.addEventListener("click", function (e) {
        e.preventDefault();
        logUserOut(e);
    });
    accountSettings.addEventListener("click", function (e) {
        e.preventDefault();
        toggleModal();
        loadSettingsModalData();
    });
};

// Clears the notebooks table in the UI.
const clearNotes = () => {
    const ulElem = document.querySelector("tbody");
    while (ulElem.firstChild) {
        ulElem.removeChild(ulElem.firstChild);
    }
};

// we need to remove the note from the user object
const deleteUserNote = (noteID) => {
    let noteDeleted = false;
    const user = getActiveUser();
    user.notes.some(function (note, index) {
        if (note.id === noteID) {
            if (confirm(`This will permanently delete the selected User NoteBook called ${note.noteBookName}. Are you sure you want to proceed?`)) {
                user.notes.splice(index, 1);
                const stringy = JSON.stringify(user);
                localStorage.setItem(getActiveUser(true), stringy);
                toastr.success("Notebook successfully removed.");
                noteDeleted = true;
            } else {
                toastr.info("The Notebook has NOT been removed.");
            }
        }
    });
    return noteDeleted;
};

// This function saves the note being edited.
const saveEditedNote = () => {
    const modal = document.querySelector(".editnoteModal");
    const ulElement = modal.querySelector("#notes");
    const listOfLI = modal.querySelectorAll("#notes li input");
    const noteNameInput = modal.querySelector("#editedNoteBookName");
    if (noteNameInput.value !== "") {
        const activeUser = getActiveUser();
        if (activeUser) {
            let position;
            activeUser.notes.some(function (note, index) {
                if (note.id === editedNoteID) {
                    position = index;
                }
            });
            activeUser.notes[position].notebookName = noteNameInput.value;
            let referenceIndex = "";
            for (let index = 0; index < listOfLI.length; index++) {
                const item = listOfLI[index];
                const newUserItem = {
                    itemName: "",
                    isCompleted: false
                }
                newUserItem.itemName = item.value;
                newUserItem.isCompleted = item.classList.contains("strike");
                activeUser.notes[position].items[index] = newUserItem;
                referenceIndex = index;
            }
            if (listOfLI.length < activeUser.notes[position].items.length) {
                activeUser.notes[position].items.splice(referenceIndex + 1, activeUser.notes[position].items.length - 1);
            }
            // we convert the user to string and store it.
            const stringy = JSON.stringify(activeUser);
            const userID = getActiveUser(true);
            localStorage.setItem(userID, stringy);
            toastr.success("Saved Successfully!");
            var ulElem = modal.querySelector("#notes");
            while (ulElem.firstChild) {
                ulElement.removeChild(ulElem.firstChild);
            }
            modal.classList.toggle("show-modal");
            clearNotes();
            loadUserNotes();
            return;
        }
        toastr.error("No Active user Found!");
        modal.classList.toggle("show-modal");
        return;
    }
    toastr.error("Note Name cannot be empty!");
    return
};

// Renders the Edit Note Form on the page.
const drawEditNoteForm = () => {
    const modal = document.querySelector(".editnoteModal");
    const ulElement = modal.querySelector("#notes");
    const listOfItems = modal.querySelectorAll("#notes li");
    const saveNoteButton = modal.querySelector("#saveEditedNote");
    const noteNameInput = modal.querySelector("#editedNoteBookName");
    // we reset the form
    listOfItems.forEach(item => {
        ulElement.removeChild(item);
    });
    saveNoteButton.addEventListener("click", function (e) {
        e.preventDefault();
        const modal = document.querySelector("#editNote");
        if (modal.classList.contains("show-modal")) {
            saveEditedNote();
        }
    });
}

// Toggles the edit note modal
const toggleEditNoteModal = () => {
    const modal = document.querySelector(".editnoteModal");
    modal.classList.toggle("show-modal");
    drawEditNoteForm();
};

// returns the active user from storage, if true is passes it returns the active user ID.
const getActiveUser = (returnID) => {
    let userID = localStorage["ActiveSession"];
    if (userID.length > 15) {
        userID = userID.slice(1, -1);
    }
    if (returnID) {
        return userID;
    } else {
        return getUserFromStorage(userID);
    }

}

let editedNoteID = "";
// A function that loads the user note and its note items into a modal for edition
const editUserNote = (noteID) => {
    toggleEditNoteModal();
    const user = getActiveUser();
    const modal = document.querySelector(".editnoteModal");
    const notesList = modal.querySelector("#noteList");
    const noteIDElem = modal.querySelector("#noteID");
    user.notes.some(function (note, index) {
        if (note.id === noteID) {
            const noteBookName = modal.querySelector("#editedNoteBookName");
            noteBookName.value = note.notebookName;
            editedNoteID = note.id;
            noteIDElem.innerText = note.id;
            let i = 0;
            for (const note of user.notes[index].items) {
                addNewNoteItem(modal);
                const listItemElems = notesList.querySelectorAll("li");
                const noteElemen = notesList.querySelectorAll("li input");
                const doneButton = listItemElems[i].querySelectorAll('button');
                noteElemen[i].value = note.itemName;
                if (note.isCompleted) {
                    doneButton[2].innerText = "Reset";
                    noteElemen[i].classList.add("strike");
                }
                i++;
            }
        }
    });

};

// loads the user notes into the dashboard notebooks table
const loadUserNotes = () => {
    const user = getActiveUser();
    const notesTable = document.querySelector("tbody");
    user.notes.slice().reverse().forEach(notebook => {
        const newRow = document.createElement("tr");
        const tdName = document.createElement("td");
        const tdDate = document.createElement("td");
        const tdActions = document.createElement("td");
        const noteName = document.createElement("a");
        const noteEdit = document.createElement("i");
        const noteDelete = document.createElement("i");

        // we add the classes
        noteEdit.classList.add("la");
        noteEdit.classList.add("la-pencil");
        noteDelete.classList.add("la");
        noteDelete.classList.add("la-trash");
        newRow.classList.add("trHover");

        // We add the values to the new row Item
        noteName.innerText = notebook.notebookName;
        tdDate.innerText = notebook.dateCreated;

        // we set the notebookName Attribute
        noteName.setAttribute("href", "#");
        noteName.setAttribute("id", notebook.id);

        // we add the child to their parent
        newRow.appendChild(tdName);
        tdName.appendChild(noteName);
        newRow.appendChild(tdDate);
        newRow.appendChild(tdActions);
        tdActions.appendChild(noteEdit);
        tdActions.appendChild(noteDelete);
        notesTable.appendChild(newRow);

        // We add the event listeners for each notebook
        noteEdit.addEventListener("click", function (e) {
            const targetEditButton = e.target;
            const targetTD = targetEditButton.parentElement;
            const targetRow = targetTD.parentElement;
            editUserNote(notebook.id);
        });

        noteName.addEventListener("click", function (e) {
            const targetEditButton = e.target;
            const targetTD = targetEditButton.parentElement;
            const targetRow = targetTD.parentElement;
            const noteNameElem = targetRow.querySelector("td a");
            editUserNote(notebook.id);
        });

        noteDelete.addEventListener("click", function (e) {
            const targetEditButton = e.target;
            const targetTD = targetEditButton.parentElement;
            const targetRow = targetTD.parentElement;
            const notesTable = document.querySelector("tbody");
            const noteNameElem = targetRow.querySelector("td a");
            let id = noteNameElem.getAttribute("id");
            let wasTheNoteDeleted = deleteUserNote(id);
            if (wasTheNoteDeleted) {
                notesTable.removeChild(targetRow);
            }
        });
    });
};

// Our "router" function takes care of drawing the selected page by the user.
const updatePage = pageName => {
    const appDiv = document.getElementById("app");
    switch (pageName) {
        case "home":
            appDiv.innerHTML = "";
            drawHomePage();
            break;
        case "login":
            appDiv.innerHTML = "";
            drawLoginForm();
            drawFooter();
            break;
        case "register":
            appDiv.innerHTML = "";
            drawRegistration();
            drawFooter();
            break;
        case "dash":
            appDiv.innerHTML = "";
            drawDashboard();
            drawFooter();
            loadUserNotes();
            setupModalEvents();
            setupNotesModalEvents();
            setupEditNoteModalEvents();
            break;
        default:
            break;
    }
};

// Calls the requested home Page.
const homePage = () => {
    updatePage("home");
};

// Draws the login form on the page.
const drawLoginForm = () => {
    const appDiv = document.getElementById("app");
    const form = document.createElement("form");
    const h1 = document.createElement("h1");
    const description = document.createElement("p");
    const username = document.createElement("input");
    const password = document.createElement("input");
    const logInButton = document.createElement("button");
    const logInCancelButton = document.createElement("button");
    const error = document.createElement("p");

    // we Set the values, classes, attributes
    h1.innerHTML = `${appMetaData.name}`;
    description.innerHTML = `Please type your <strong>userName</strong> and <strong>Password</strong> to Login.`;
    description.style.fontSize = "12px";
    description.style.margin = "10px";
    logInButton.innerText = "Log In";
    logInButton.classList.add("login");
    username.setAttribute("id", "userName");
    username.setAttribute("placeholder", "username");
    password.setAttribute("placeholder", "password");
    password.setAttribute("id", "userPassword");
    password.setAttribute("type", "password");
    logInCancelButton.innerText = "Cancel";
    logInCancelButton.classList.add("cancel");
    description.style.textAlign = "center";
    error.innerText = "ERROR: incorrect username or parssword."
    error.setAttribute("hidden", true);
    error.classList.add("loginError");

    // we append the elements to their parents
    form.appendChild(h1);
    form.appendChild(description);
    form.appendChild(username);
    form.appendChild(password);
    form.appendChild(logInButton);
    form.appendChild(logInCancelButton);
    appDiv.appendChild(form);
    appDiv.appendChild(error);

    // we set up the events
    logInButton.addEventListener("click", function (event) {
        event.preventDefault();
        logUser();
    });
    logInCancelButton.addEventListener("click", (e) => {
        e.preventDefault();
        homePage();
    });
};