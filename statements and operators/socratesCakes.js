/*
    All men are mortal
    Socrates is a man.
    Therefore, socrates is mortal.
*/
function isMortal(personName) {
    // This could've been declared outside the function-block to have it as a global variable.
    let men = {
        menNames: ['Plato', 'Socrates', 'Pericles', 'Pythagoras' ],
        mortal: true,
    }

    if (men.mortal && men.menNames.includes(personName)) {
            // If all men are mortal & Socrates is a man. Then socrates is mortal.
            console.log(`All men are mortal: ${men.mortal}`);
            console.log(`Socrates is a man: ${men.menNames.includes(personName)}`);
            console.log(`Therefore, ${personName} is mortal.`)
    } else { 
        console.log(`Either ${personName} is not a man or Not all men are mortal.`)
    }
}

// We test the function
isMortal("Socrates");
isMortal('Osiris');

console.log("\n");

/* 
    This cake is either vanilla or chocolate.
    This cake is not chocolate.
    Therefore, this cake is vanilla.
*/

// we define the valid cake flavors
let cakeFlavors = ['Vanilla', 'Chocolate'];

// and we create some cakes
let cake = { flavor: "Vanilla" };
let otherCake = {flavor: "Lemmon"};
let yetAnotherCake = {flavor: "Chocolate"};

// then we check what flavor a given cake is
function whatsTheFlavor(cake) {

    // Cakes are either vanilla or chocolate.
    if (cakeFlavors.includes(cake.flavor)) {
        // if the cake is not chocolate
        if (cake.flavor !== cakeFlavors[1]) {
            // the it is vanilla
            console.log(`This cake is ${cake.flavor}`);
        } else {
            // otherwise it is chocolate
            console.log(`This cake is ${cake.flavor}`);
        }
    } else { 
        // or not a valid flavor at all
        console.error(`${cake.flavor} is not a valid cake flavor.`)
    }
}

// we test the function
whatsTheFlavor(cake);
whatsTheFlavor(otherCake);
whatsTheFlavor(yetAnotherCake);
