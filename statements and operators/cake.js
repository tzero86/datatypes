/* 
    This cake is either vanilla or chocolate.
    This cake is not chocolate.
    Therefore, this cake is vanilla.
*/

class cake {
    constructor(){
        this.isVanilla = false;
        this.isChocolate = false;
        
    }

    pickFlavor(flavor) {
        console.log("This is the flavor received: ", flavor);
        if (flavor === "vanilla") {
            this.isChocolate = false;
            this.isVanilla = true;
        } else if(flavor === "chocolate"){
            this.isChocolate = true,
            this.isVanilla = false;
        }
    }
}

// then we create a new cake
let myCake = new cake();
let anotherCake = new cake();
let yetAnotherCake = new cake();

myCake.pickFlavor("vanilla");
anotherCake.pickFlavor("chocolate");
yetAnotherCake.pickFlavor("Lemmon");

function checkFlavor(cake) {
    if (cake.isChocolate || cake.isVanilla) {
        if (!cake.isChocolate) {
            console.log("My Cake is Vanilla");
        } else if (cake.isChocolate){
            console.log("My Cake is Chocolate");
        }
    } else {
        console.error("Incorret Flavor specified!");
    }
}

checkFlavor(myCake);
checkFlavor(anotherCake);
checkFlavor(yetAnotherCake);



