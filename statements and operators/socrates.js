/* 
    All men are mortal
    Socrates is a man.
    Therefore, socrates is mortal. 
*/

// we difine a new object to represent men's mortality
const men = {
    allMenAreMortal: true
}

// then we create another representing socrates
let socrates = {
    name: "Socrates",
    age: 71,
    isMortal: false,
    isAMan: true,
}


// If all men are mortal
if (men.allMenAreMortal && socrates.isAMan) {
    console.log("All Men are Mortal and socrates is a Man")  
    // since it is a man we print out the logs and set socrates' isMortal to true
    console.log("Then Socrates is mortal.");
    socrates.isMortal = true;
}


// A different solution with ES6 classes could be

// we define men
class allMen {
    constructor(name, age){
        this.name = name;
        this.mortal = true;
        this.age = age;
    }
}

// we define a man
class oneMan extends allMen {
    constructor(name, age){
        super();
        this.name = name;
        this.age = age;
        this.isAMan = true;
        this.from = "Greece"    
    }
}

// we create humans and a man called Socrates
let allMenOfHearth = new allMen("Humans", 300000);
let socrates2 = new oneMan("Socrates", 71);

// if all men are mortal, we ask if socrates is a man
if (allMenOfHearth.mortal && socrates2.isAMan) {
    // we also could avoid checking if Socrates is mortal, since we know socrates inherited all properties of allMen class.
    if (socrates2.mortal) {
        console.log("Socrates is a Man and all men are mortal, therefore Socrates is mortal.");
    } else { console.log("Socrates is not mortal."); }
} else { console.log("Socrates is not a Man."); }
 

// We print out the entire set of properties of all men and Socrates.
console.log(allMenOfHearth, socrates2);

