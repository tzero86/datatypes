/*
    Priple's Keeping Up with the JavaScripts course
    HomeWork #6: Fizz Buzz Prime Numbers exercise.
*/ 

// We start by creating a function to check if a num is prime.
const isItAPrimeNumb = (num) => {
    for (let index = 2; index < num; index++){
        if (num % index === 0) {
            return  false;
        }
    }
    return num > 1;
};


// then we create our main looper function
const fizzBuzzPrime = () => {
    for (let index = 1; index <= 100; index++) { // we loop from 1 to 100
        if (isItAPrimeNumb(index)) { // first we check if it is a prime numb
            console.log("Prime");
        } 
        else if (index % 15 === 0) { // we check it it is multiple of both 5 and 3
            console.log("FizzBuzz");
        }
        else if (index % 3 === 0) { // else, we check if it is multiplle of 3
            console.log("Fizz");
        }
        else if (index % 5 === 0) { // else, we check if it is multiplle of 3
            console.log("Buzz");
        }
        else { // if the number is not fizz, buzz nor Prime we print it out as is.
            console.log(index);
        }
    }
}

// Finally we call our main function and see if it works.
fizzBuzzPrime();