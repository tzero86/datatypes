class Passenger{
    constructor(originFloor, targetFloor, elevator){
        this.originFloor = originFloor;
        this.targetFloor = targetFloor;
        this.elevator = elevator;
    }
    callElevator(){
        console.log("callElevator() function");
    }
    getOnElevator() {
        console.log("getOnElevator() function");
    }
    getOffElevator() {
        console.log("getOffElevator() function");
    }
}

class Elevator {
    constructor(name = null, floors = [], unavailableFloors = []){
        this.name = name;
        this.floors = floors;
        this.unavailableFloors = unavailableFloors;
        this.currentFloor = 5;
        this.nextFloor = null;
        this.hasPassenger = false;
        this.isMoving = false;
        this.isEmergencyOn = false;
    }
    openDoors() {
        console.log("openDoors() function");
    }
    closeDoors() {
        console.log("closeDoors() function");
    }

    moveToFloor(passenger) {
        if(!this.isMoving){
            this.nextFloor = passenger.originFloor;
            // TODO: need to think about this logic.
        }
    }

    emergencyMode() {
        console.log("emergencyMode() function");
    }

    resetElevator() {
        console.log("resetElevator() function");
    }
}

const buildingFloors = [-1,0,1,2,3,4,5,6,7,8,9,10];
const elevatorA = new Elevator("A", buildingFloors, [10]);
const elevatorB = new Elevator("B", buildingFloors, [-1]);
let passenger1 = new Passenger(2, 10, "A");
let passenger2 = new Passenger(-1, 6, "B");
//console.table(elevatorA);
//console.table(elevatorB);
//console.table(passenger1);
//console.table(passenger2);

//elevatorA.moveToFloor(passenger1);


Vue.component("building-location", {
    props: ['floors', 'callelev', "goingup", "goingdown"],
    template:
    `<div class="container">
        <table id="building">
            <thead>
                <th>Floor</th>
                <th>A</th>
                <th>Actions</th>
                <th>B</th>
                <th>Floor</th>
            </thead>
            <tbody>
                <tr v-for="floor in floors.slice().reverse()">
                    <td v-bind:id="floor" class="floor" >{{floor}}</td>
                    <td class="elevator"><i v-if="floor == 0" class="fas fa-columns" title="Elevator A"></i></td>
                    <td class="floor actions" >
                        <!-- <a href="#" title="Call Elevator" class="button" @click="callelev"><i class="fas fa-bell"></i></a> -->
                        <a v-if="floor <10" href="#" title="Going Up" class="button" @click="goingup"><i class="fas fa-caret-square-up"></i></a>
                        <a v-if="floor >=0" href="#" title="Going Down" class="button" @click="goingdown"><i class="fas fa-caret-square-down"></i></a>
                    </td>
                    <td class="elevator"><i v-if="floor == 0" class="fas fa-columns" title="Elevator B"></i></td>
                    <td class="floor">{{floor}} </td>
                </tr>
            </tbody>
        </table>
    </div>`,
});
Vue.component("travels-queue", {
    props: ['travels'],
    template:
    `
    <div class="queue">
        <table>
            <thead>
                <th>Elev</th>
                <th>Orig</th>
                <th>Dest</th>
            </thead>
            <tbody id="queue">
                <tr v-for="travel in travels">
                    <td class="" >{{ travel.elevatorAssigned }}</td>
                    <td class="">{{ travel.origin }}</td>
                    <td class="">{{ travel.destination }}</td>
                </tr>
            </tbody>
        </table>
    </div>`,
});

const app = new Vue({
    el: '#app',
    data: {
        buildingFloors: buildingFloors,
        elvA: elevatorA,
        elvB: elevatorB,
        travelsQueue: [],
    },
    mounted: function () {
        this.$nextTick(function () {
            // Code that will run only after the
            // entire view has been rendered
            console.table(this.elvA);
            console.table(this.elvB);
            this.elvA.currentFloor = 8;
            this.elvB.currentFloor = 0;
        });
    },
    methods: {
        callElevator(e, direction){
            let target = e.currentTarget.parentNode.parentNode;
            let floorID = target.querySelector("td");
            originFloorID = floorID.innerText;
            console.log(target);
            console.log(originFloorID);
            let destinationFloor = this.selectFloor(direction, originFloorID);
            console.log(destinationFloor);
            this.assignToElevator(originFloorID, destinationFloor);
        },
        selectFloor(direction, originFloorID){
            let choice = -5;
            let availableFloors = new Array();
            availableFloors = this.buildingFloors.filter(floor => floor != originFloorID);
            if (direction == "up") {
                availableFloors = availableFloors.filter(floor => floor > originFloorID);
            } else if (direction == "down") {
                availableFloors = availableFloors.filter(floor => floor < originFloorID);
            }
            console.log(`Elevator Called from floor number ${originFloorID}, since the resident wants to go ${direction} the floors available for selection are: ${availableFloors}`);
            console.table(availableFloors);

            while (availableFloors.indexOf(choice) == -1) {
                choice = prompt(`Elevator Called from floor number ${originFloorID}, since the resident wants to go ${direction} the floors available for selection are: ${availableFloors}`);
                console.log(choice);
                choice = parseInt(choice);
                console.log(`Does the available floors includes the user choice? ${availableFloors.includes(choice)}`)
                if (availableFloors.includes(choice)) {
                    break;
                }
            }
            return choice;
        },
        assignToElevator(originFloorID , destinationFloor) {
            // FIXME: need to clean this code up and refactor it.
        /*  if (destinationFloor == -1 && originFloorID != 10) {
                console.log("Passenger added to Elevator B queue.");
                this.elvB.nextFloor=destinationFloor;
                console.table(this.elvB);
                return;
            } else if (destinationFloor == 10 && originFloorID != -1) {
                console.log("Passenger added to Elevator A queue.");
                this.elvA.nextFloor=destinationFloor;
                console.table(this.elvA);
                return;
            } else {
                if(originFloorID == 10){
                    console.log("You can't travel from the penthouse to the basement!, you'll be picked up by ElevatorB and can go down to the lobby inclusive.");
                    this.elvB.nextFloor=destinationFloor;
                    console.table(this.elvB);
                return;
                } else if (originFloorID == -1){
                    console.log("You can't travel from the basement to the penthouse!, you'll be picked up by ElevatorA and can go up to floor 9 inclusive.");
                    this.elvA.nextFloor=destinationFloor;
                    console.table(this.elvA);
                    return;
                }
                console.log("Passenger will be added to the queue of the closest elevator(logic to be added).");
                // TODO: Create function to calculate which elevator is closest to the passenger and then assign the passenger to that elevator. */
                this.getClosestElevator(originFloorID, destinationFloor);
            //}
        },
        callElevatorUp(e) {
            console.log('Going Up!');
            this.callElevator(e, "up");
        },
        callElevatorDown(e){
            console.log('Going Down!');
            this.callElevator(e, "down");
        },
        getClosestElevator(originFloorID, destinationFloor) {
            let closest;
            let newTravel = {
                origin: originFloorID,
                destination: destinationFloor,
                elevatorAssigned: null
            };
            let distanceA = this.getDistance(this.elvA, originFloorID);
            let distanceB = this.getDistance(this.elvB, originFloorID);

            if (distanceA <= distanceB) {
                closest = "A";
                newTravel.elevatorAssigned = "A";
                this.addToTravelQueue(newTravel);
            } else if (distanceB <= distanceA) {
                closest = "B";
                // TODO: we can actually create another fucntion that adds a new travel to the stack
                newTravel.elevatorAssigned = "B";
                this.addToTravelQueue(newTravel);
            } else {
                closest = "A";
                newTravel.elevatorAssigned = "A";
                this.addToTravelQueue(newTravel);
            }
            return closest;
        },
        getDistance(elevator, originFloorID) {
            let distance;
            if (elevator.currentFloor == originFloorID) {
                distance = 0;
            } else {
                if (originFloorID < elevator.currentFloor) {
                    distance = elevator.currentFloor - originFloorID;
                    console.log(`Elevator ${elevator.name} is at floor number ${elevator.currentFloor} and The Passenger is ${distance} floor(s) below.`);
                } else if (originFloorID > elevator.currentFloor) {
                    distance = originFloorID - elevator.currentFloor;
                    console.log(`Elevator A is at floor number ${elevator.currentFloor} and The Passenger is ${distance} floor(s) Above.`);
                }
            }
            return distance;
        },
        addToTravelQueue(travel) {
            //TODO: need to create the queue for all the travels between floors.
            console.table(travel);
            this.travelsQueue.push(travel);
        },
    },
});