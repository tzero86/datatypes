
// we grab the reset button.
const resetBttn = document.getElementById("newGame");
// we grab the main div container
const appDiv = document.getElementById("app");
// we  then grab all the boxes representing the board
const boxes = appDiv.querySelectorAll("div");

let player = 1; // by default player 1 starts
let gamePlayable = true; // false when a player wins.

// Represents valid combinations to win: Rows, columns and lastly diagonals.
const winCombos = [ [0,1,2],[3,4,5],[6,7,8] ,[0,3,6],[1,4,7],[2,5,8] ,[2,4,6],[0,4,8] ];
let currentGame = [0,0,0,0,0,0,0,0,0] // we start the board tracker at 0 which means the board is untouched by any player.


// a simple function to remove player styles and reset the board
const resetBoard = () => {
    for (const prop of boxes) {
        prop.classList.remove("player1");
        prop.classList.remove("player2");
        prop.innerText =".";
    }
    currentGame = [0,0,0,0,0,0,0,0,0];
    player = 1;
    gamePlayable = true;
    console.log("The board has been reset!");
};


// A function to trigger an alert informing the winner
const showWinner = (playerNumber) => {

    setTimeout(()=> {
        if (playerNumber === 1) {
            alert("Player 1 has Won the game.");
        } else if (playerNumber === 2) {
            alert("Player 2 has Won the game.");
        } else {
            alert("cat's game");
        }
        resetBoard();
    }, 300);
};

// Our function to determine if either player 1 or player two made a winning combination.
// if neither player won, we check if we are still playing or if it's a draw
const checkForWinCombination = () => {
	if(gamePlayable) {
		for(let index = 0; index < winCombos.length; index++){
			if ( currentGame[winCombos[index][0]] !== 0 && currentGame[winCombos[index][0]] === 1 && currentGame[winCombos[index][1]] === 1 && currentGame[winCombos[index][2]] === 1) {
                gamePlayable = false;
                showWinner(1);
				return;
			} else if( currentGame[winCombos[index][0]] !== 0 && currentGame[winCombos[index][0]] === 2 && currentGame[winCombos[index][1]] === 2 && currentGame[winCombos[index][2]] === 2) {
                gamePlayable = false;
                showWinner(2);
				return;
            } 
            // if no winner was found we call a helper function to see if its a cat's game or we still have boxes to play
	        checkDrawOrStillPlayable();
        }
	}
};


const checkDrawOrStillPlayable = () => {
    if (gamePlayable) { // we make sure there is an active game going
        let gameEnded = true; // by default we assume the game is over
		for (const position of currentGame) {
			if (position === 0) {
				// then we still have positions left to play so we set gameEnded to false
				gameEnded = false;
			}
		}
		if (gameEnded) {
			// then we don't have any positions left to play and the game is over(draw)
			gamePlayable = false;
			showWinner();
		}
	}
};

// We update our current game array to track the player moves
const updateBoard = (id) => {
    if(gamePlayable && currentGame[id] === 0) {
        if(player === 1) {
            currentGame[id] = 1;
            player = 2;
        } else if (player === 2) {
            currentGame[id] = 2;
            player= 1;
        }
        console.log(`Board Status: ${currentGame}`);
        checkForWinCombination(); // we check if there is a winner yet
    }
};


// We check which player made the move and update the board accordingly
const checkPlay = (e) =>{
    const clickedBox = e.target;
    const boxID = clickedBox.id;
    console.log(`Box clicked: ${boxID}`);
    if(clickedBox.innerText === ".") {
        if(player == 1) {
            clickedBox.innerText = "X";
            clickedBox.classList.add("player1");
            updateBoard(boxID);
        } else if (player == 2) {
            clickedBox.innerText = "O";
            clickedBox.classList.add("player2");
            updateBoard(boxID);
        }
    } else { console.log("box already clicked!")}
};

// We add the event listener to all of the boxes.
for (const prop of boxes) {
    prop.addEventListener("click", checkPlay);
}

// we add an event Listener to the reset button
resetBttn.addEventListener("click", resetBoard);
