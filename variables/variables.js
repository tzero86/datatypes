/*
var: is the traditional way of declaring variables in JS (ES5 and before), but it has some downsides:
    .Variables can be accessed outside the scope
    .Variables intended to be used as constant values can be re-defined
    .Variables are function scoped
*/

// We can declare a variable that we might want to change later like this one
var isThisRealLife = true;
console.log(isThisRealLife = false);

// but some other times we might want to store a value and make sure it never changes
var pi = Math.PI;

// however due to the nature of var declarations, those cases can be re-defined
function iJustAlterThePI(){
    pi = "pie";
}

// If we console log our pi variable at this point, it will contain the expected PI value
console.log(`The value of PI is: ${pi}` )


// but if we call our alter-pi function and then we console.log pi, the value will be changed.
// and we might not want to do that
iJustAlterThePI();
console.log(`The value of PI is: ${pi}` )

// Globally scoped variable.
var isThisNotGood = false

// Out Litte function
function grabTheVariable() {
    if (!isThisNotGood) {
        var isThisNotGood = true;
        console.log("Log From Inside the Function's IF block: " , isThisNotGood)
    }
    // we can still access and reassing the isThisNotGood variable outside of if block.
    isThisNotGood = "oh god";
    console.log("Log from outside the function's IF block: " , isThisNotGood)
}

// We call our grabTheVariable function and see what it prints out to the console.
grabTheVariable();

console.log("Log from outside the function scope: ", isThisNotGood);

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*
    with ES6 we can have now block-scoped variable declarations
    Let are blocked-scoped variabled, which lets us have more control over the scope of the variables and limit scope-errors
    I personally don't see any reason to keep using var while having let, unless you really need function-scoped variables.
*/      

// we can still use let to define globally-scoped variables
let isThisATest = true;

// we can still re-assing those variables
isThisATest = "yes";
console.log("Is this a Test sir? " , isThisATest);


// but we do have more control over the scope of those variables.
function whatAScope() {
    // I can still print out the global variable defined above
    console.log("I can access that glob variable: ", isThisATest);

    if (isThisATest) {
        // however if I declare another variable here inside the if block
        let isThisAJoke = false;
    }

    // We do a Try-catch to test out the scope
    try {
        // I should get an error If I try to access that If-Scoped variable, which is very useful 
        // when you want to have control over the scope of the variables.
        console.log(isThisAJoke);

    } catch (error) { console.error("You tried to access a variable outside of its block-scope! ", error); }
}

// We call out function to see what happens.
whatAScope();

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*
    lastly we have const declarations, which are values that once-declared and assigned, cannot be changed.
    This is ideal for when you need to have some settings or values defined so they would never change along the program execution
*/

// We can declare constants which are variables that can't be modified after being declared and assigned.
const theValueOfPI = Math.PI;

// if we try to alter the value of theValueOfPI we should get an error
function alterTheConstPI() {
    try {
        // We try to alter the value of the constant
        theValueOfPI = 10;
    } catch (error) {
        // and we should get an error
        console.error("You tried to alter the value of a constant! ", error)
    }
}

// we call our function
alterTheConstPI();

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


/*
    HOISTING: "Hoisting is a JavaScript mechanism where variables and function declarations are moved to the top of their scope before code execution. 
    Inevitably, this means that no matter where functions and variables are declared, they are moved to the top of their scope 
    regardless of whether their scope is global or local." source: https://scotch.io/tutorials/understanding-hoisting-in-javascript
*/

// JS by default hoists the value declarations, so when we do this
var hello = "hi";

// JS will internally do the declaration first
var hello;
// and then the assignment
hello = "hi"

// if we directly assign a variable without declaring it
// it will implicitly create a global-scoped variable when that line is executed
function houseOfMine() {
    let thisIsSoCool = "sure";
    house = "my house";
    console.log("hey This is my function-scoped thisIsSoCool variable, printing out: ", thisIsSoCool);
}

//Therefore undeclared variables will always be converted to global variables
houseOfMine();
console.log(house);

// While if we want to access the thisIsSoCool variable outside of the function scope
// we should get an error.

try {
    console.log(thisIsSoCool);
} catch (error) {
    console.error("You tried to access a variable thats been declared inside of a function. Therefore its scope is limited to that function ", error);
}



