/*
    Pirple's ES6 Course.
    Tzero86

    Destructuring is an ES6 feature that lets us
    extract values from data stored in arrays and objects.

    Arrays - For arrays destructuring must use square brackets[]. In Arrays position of the elements is key. In other words, if you destructure an array you need to take the position of the value in the array into account. You can also skip selected values as long as you consider the positions.
            Example:
                const array1 = [1,2,3];
                const [,,b] = array1; // we skip position 0 and 1
                console log(b); // prints out the value at position num 2, which is 3

    Objects - For object destructuring, you must use curly braces {} and can't start a statement with curly braces. In those cases you need to wrap the statement in between parenthesis () for it to work.  The positions of the values is not important. But unless you assing alias to the keys, you have to respect the keyNames present in the object (key-value pairs). In other words, if you destructure an object you access only the properties that you need, using curly braces and the order in which you do it does not matters.

            Example:
                const myObject = { name: "Jack", lastName: "of Spades"};
                const { lastName } = myObject; // we ignored the name, and accessed just lastName
                console.log(lastName); // prints out "of Spades"

    Destructuring also works on nested objects.

    Destructuring can be applied when:
        - Declaring Variables
        - Assigning values
        - Parameters
        - Loops (for Of)
        - Swapping values
        - Defining default values
*/

// Array Example 1
const array1 = [1,2,3];
const [,,b] = array1; // we skip position 0 and 1
console.log(b); // prints out the value at position num 2, which is 3

// Object Example 1
const myObject = { name: "Jack", lastName: "of Spades"};
const { lastName } = myObject; // we ignored the name, and accessed just lastName
console.log(lastName); // prints out "of Spades"

// we use destructuring and default in a function
function myNameIs({name = "default", age}){
    console.log(`My name is ${name}, and I am ${age} years old.`);
};

myNameIs({name: "Elias", age: 32}); //  a complete object
myNameIs({age: 43}); //  an incomplete object so we test the default
myNameIs({}); //  an fully incomplete object so we test the default and undefined

// with functions we can also do it like this
function myOtherNameIs(userDetails){
    const { name = "N/A", age = "N/A" } = userDetails;
    console.log(`My Other name is ${name}, and I am ${age} years old.`);
}

myOtherNameIs({name: "Richard", age: 45 }); // A complete object
myOtherNameIs({}); // a fully empty object, to test both defaults
myOtherNameIs({age: 45 }); // to test name default
myOtherNameIs({name: "Richard"}); // to test age default

// defaults can also be used like this
const [a,,c,d= "Not a Number"] = array1;
console.log(d); // prints out "not a number"

// we can swap values
let myValue1 = 1;
let myValue2 = 2;
[myValue1, myValue2] = [myValue2, myValue1]; // we swap the values

console.log(`Value1: ${myValue1}`); // prints out 2
console.log(`Value2: ${myValue2}`); // prints out 1

// we can also destructure nested objects
const myNestedObject = {
    students: [
        {
            name: "Ellis",
            lastName: "Jordan",
            score: "A+"
        },
        {
            name: "Tommas",
            lastName: "Lyle",
            score: "C+"
        }
    ],
    isSchoolYearActive: true,
    schoolPricipalsActiveYears: [
        ["1986-2000", ["Peter, McAllister", "Susan, Miller"]], // years active Principal, Assistant
        ["2000 - 2001", ["Susan, Miller", "Peter, McAllister"]],
        ["2001 - Current", ["Susan, Miller", "David, Trout"]],
    ]
};

const {schoolPricipalsActiveYears} = myNestedObject;
console.log(schoolPricipalsActiveYears);

// We can still retrieve data from a nested object
({students: [{name: theName, lastName: surname}], isSchoolYearActive : isActive} = myNestedObject);
console.log(`The first Student name is: ${theName}, ${surname}. And the School Year is ${isActive ? "active so he must be at school." : "finished so he must be on vacations."}`);

// but we can also destructure in a loop
const { students: entries } = myNestedObject; // we opt to select only the students property to a new alias
for (const {name, lastName, score} of entries) { // we select the values we want from each entry
    console.log(`The Academy's Student name is: ${name}, ${lastName}. Score: ${score}.`);
}

// Let's check the array
for (const [period, personnel] of myNestedObject.schoolPricipalsActiveYears) {
    console.log(`During the period ${period}, the Principal was ${personnel[0]} and the assistant was ${personnel[1]}.`);
}