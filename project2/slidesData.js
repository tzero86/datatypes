const slidesData = [
    {
        id: 0,
        title: "Welcome!",
        content: `
            <div style="text-align:center;">
            Welcome to the VueJS introduction, during this presentation we'll cover the basics to get started with VueJS. Since this is intended to be an introduction to VueJS, we won't cover Vue CLI and several other advanced features. However, at the end of this psesentation you should understand enough about Vue so you can start learning about those other features and tools available for developers.<br><br>
            To navigate through these slides, you can use the <strong>buttons below the slide or the following keys</strong> that were configured:
            <br><br>
            <ul style="text-align:center;line-height: 40px;">
                <li><span>Press <strong style="font-size: 18px"><a href="#" class="button2">F</a></strong> to enable/disable Fullscreen mode. </span></li>
                <li><span>Press <strong style="font-size: 18px"><a href="#" class="button2">Space Bar</a></strong> to advance through the slides</span></li>
                <li>Press <strong style="font-size: 18px"><a href="#" class="button2">Left-Arrow</a></strong> or <strong style="font-size: 18px"><a href="#" class="button2">Right-Arrow</a></strong> to go to the previous/next slide. </li>
                <li>Press <strong style="font-size: 18px"><a href="#" class="button2">+</a></strong> or <strong style="font-size: 18px"><a href="#" class="button2">-</a></strong> to decrease/increase the font size.</li>
            </ul>
            <br><br>
            That's it!, I hope you enjoy the presentation and that it gets you interested in learning VueJS.
            <br>
            <br>
            <p style="opacity:0.4; text-align:center; font-style: bold; font-size: 15px;">(When you are ready, advance to the next slide to start)</p>
            </div>
        `,
        image: '',
    },
    {
        id: 1,
        title: "What is VueJS?",
        content: `
        <blockquote>
        Vue (pronounced /vjuː/, like view) is a progressive framework for building user interfaces. Unlike other monolithic frameworks, Vue is designed from the ground up to be incrementally adoptable. The core library is focused on the view layer only, and is easy to pick up and integrate with other libraries or existing projects. On the other hand, Vue is also perfectly capable of powering sophisticated Single-Page Applications when used in combination with modern tooling and supporting libraries.- from Vuejs.org
        </blockquote>
        <div style="text-align:center;">During the following slides we'll learn the basics of working with VueJS</div>.
        <br>
        <br>
        <img src="./img/slide_01.png"></image>
        <br>
        <p style="opacity:0.4; text-align:center; font-style: bold; font-size: 15px;">(When you are ready, advance to the next slide to start learning)</p>
        `,
        image: '',
    },
    {
        id: 2,
        title: "Getting Started with VueJS",
        content: ` The are a few ways you can get started with VueJS. However, we'll cover the simplest of them which is ideal for anyone with basic understanding of HTML, CSS and JavaScript.
        <br><br>
        The easiest way to try out Vue.js is by creating an index.html file and include Vue with the following script tag:
        <br>
        <br>
        <img src="./img/slide_02.jpg"></image>
        <br>
        Considering this we can create a basic index.html file for our little project. We would also need to add a <strong>div tag</strong> inside the <strong>body tag</strong> that will be used as the container for our VueJS app. It is worth mentioning that this container tag is required so Vue can render our application inside that tag.
        <br><br>
        The last thing we'll need is to create a <strong>app.js</strong> in the same folder where the <strong>index.html</strong> This file will contain all the logic of the application and to use it we must also load it in its own <strong>script tag</strong>.
        <br><br>
        The resulting file would look like this:
        <br>
        <br>
        <img src="./img/slide_02_basicHTML.jpg"></image>
        <br>
        With this file and the <strong>app.js</strong> files ready it is now time for our first contact with Vue.<br><br>
        <div style="opacity:0.4; font-style: bold; font-size: 15px; text-align:center;">(When you are ready, advance to the next slide to continue)</div>
        `,
        image: ''
    },
    {
        id: 3,
        title: "The Vue Instance",
        content: `
        Remember in the last slide we created that <strong>div tag</strong> that we said it would be in charge of providing a place in our index.html file for our Vue app to be rendered? Well it is now time to talk a bit more about it.
        <br>
        <br>
        VueJS needs a tag in our HTML file to act as a container where our application will live. It is important to mention that this container will let Vue monitor anything that occurs inside it.
        <br>
        <br>
        <blockquote>
        This means that any Vue-specific-syntax that we use in our projects will only be correctly interpreted by Vue if they are placed inside the container tag to which our Vue app has been "linked" to.
        </blockquote>

        <h2>The Vue Instance: First Contact</h2>
        A Vue application always starts by creating a new <strong>Vue Instance</strong> by using the <strong>Vue function</strong>. A Vue application consists of a <strong>root or main Vue Instance</strong> created by the <strong>Vue function</strong> and of <strong>components</strong> that we can user to organize our application intro controlled maintainable parts that we can reuse whenever we need to. We'll touch on <strong>components</strong> later, but just bear in mind they are part of what makes Vue quite powerful and easy to adopt.
        <br>
        <br>
        <blockquote>
        If you are wondering where does that <strong>Vue function</strong> comes from, it's from the script tag containing the link to the VueJS framework file that we added to our <strong>index.html</strong> file.
        </blockquote>
        <br>
        So, let's declare our first Vue app. The first thing we need to do is to create a new Vue instance inside our <strong>app.js</strong> file using the <strong>Vue function</strong> we mentioned earlier. To do that we write it like this:
        <br>
        <br>
        <img src="./img/slide_03_VueInstance01.jpg"></image>
        <br>
        The instance receives a <strong>configuration object</strong> that we'll use to configure our application behaviors. The first thing we need to define is the <strong>el property</strong> , <strong>el</strong> stands for <strong>element</strong> and it's our way of indicating Vue where we want it to render our app. The value for this property is a string that acts as a querySelector, therefore we can target elements by class, id, etc. So we set this <strong>el property</strong> value to the <strong>id</strong> we gave to our container <strong>div element</strong> in our <strong>index.html</strong> file. If you want to know more about these selectors take a look <a target="_blank" href="https://developer.mozilla.org/es/docs/Web/API/Document/querySelector">here.</a>
        <br><br>
        With this in place we can now add a bit of data to our app and try out how it gets rendered. In Vue JS our <strong>instance's configuration object</strong> can contain a <strong>data object</strong> where we can define some test data for our component. To do so, we need to add the following to our Vue instance code block:
        <br>
        <br>
        <img src="./img/slide_03_VueInstance02.jpg"></image>
        <br>
        As you can se inside the <strong>data object</strong> we defined several pieces of data with different data types each. We can also set the value of these data properties to be arrays, etc.
        Ok, so we created our first Vue instance for our app, we've set the element under which our app will be rendered and we even added to data to our app. So you might be thinking, <strong>how do we render now that data into our HTML file?</strong> This is where data binding and text interpolation come in handy and we'll talk about them in the next slide.
        <br><br>
        <div style="opacity:0.4; font-style: bold; font-size: 15px; text-align:center;">(When you are ready, advance to the next slide to continue)</div>
        `,
        image: ''
    },
    {
        id: 4,
        title: "Data Bindings",
        content: `
        The most basic data binding we can do is using string interpolation by typing <strong>double curly braces {{ }}</strong> and placing inside them any of the properties of the <strong>data object</strong> that we defined in our <strong>Vue Instance</strong>. To do so we just need to place the name of the property inside the double curly braces in our <strong>index.html</strong> file. Like this:
        <br>
        <br>
        <img src="./img/slide_04_stringInterpolation.jpg"></image>
        <br>
        Notice in <strong>line #12</strong> how we are calling the property <strong>whatAreWeLearning</strong> that we defined in our <strong>app.js</strong> file:
        <br>
        <br>
        <img src="./img/slide_04_appjs.jpg"></image>
        <br>
        With this change to our app we have made our data property <strong>Reactive</strong>. This means that Vue will now monitor that property's value for any change made to it and will automatically update the value inside the HTML without us having to do anything else.
        <br>
        At this point we can try our app in a browser to see how it looks and to check out how Vue automatically renders the data inside the HTML.
        <br>
        <br>
        <img src="./img/slide_04_preview.jpg"></image>
        <br>
        There you have it; you already created your very first Vue app! it was a simple example but that already let us see the potential of VueJS declarative rendering.
        <br>
        <br>
        <br>
        <br>
        <div style="opacity:0.4; font-style: bold; font-size: 15px; text-align:center;">(When you are ready, advance to the next slide to continue)</div>
        `,
        image: ''
    },
    {
        id: 5,
        title: "Data Bindings II",
        content: `
        <h2>Rendering out HTML</h2>
        It is worth mentioning that you cannot render HTML inside the double curly braces as any tag you specify will be rendered like plain text. Instead if you need to display HTML you'll have to use <strong>v-html</strong> tag on an element.
        <br>
        <br>
        So if we were to add a new data attribute containing the representation of an HTML tag, like this:
        <br>
        <br>
        <img src="./img/slide_04_vhtml01.jpg"></image>
        <br>
        We would think that if we try to bind that data to our HTML by using string interpolation like this:
        <br>
        <br>
        <img src="./img/slide_04_vhtml03.jpg"></image>
        <br>
        We would get rendered in the page a new paragraph containing the text <strong>This is a paragraph</strong>, but actually the text is entirely rendered to the page. That is the whole value of the data we defined, so instead we get this:
        <br>
        <br>
        <img src="./img/slide_04_vhtml02.jpg"></image>
        <br>
        So, in order to fix this. We can use the aforementioned <strong>v-html</strong> tag in our HTML file, maybe by using a <strong>span</strong> tag to hold our data like this:
        <br>
        <br>
        <img src="./img/slide_04_vhtml04.jpg"></image>
        <br>
        And now when we refresh our <strong>index.html</strong> file, we get the expected result:
        <br>
        <br>
        <img src="./img/slide_04_vhtml05.jpg"></image>
        <br>
        <br>
        In the next slides we'll cover more VueJS features that would take our experience with the framework to the next level. We'll talk about <strong>Components, Directives, Templates and Lifecycle Hooks</strong>.
        <br><br>
        If you want to quickly try this example in your browser <strong>you can open the following <a target="_blank" href="https://scrimba.com/c/c2LD42sV">link</a></strong> it will take to you to an online code editor so you can practice around a bit with the code we went through. Go ahead and take a look at it, practice rendering some other data property, declare new ones and render them using string interpolation and try to get comfortable with the basics before continuing.
        <br>
        <br>
        <div style="opacity:0.4; font-style: bold; font-size: 15px; text-align:center;">(When you are ready, advance to the next slide to continue)</div>
        `,
        image: ''
    },
    {
        id: 6,
        title: "Directives",
        content: `
            There is a limitation with string interpolation, it cannot be used for HTML attributes. Luckily Vue offers a solution to this, called a <strong>Data Binding Attribute</strong>. One of such attributes is called the <strong>v-bind directive</strong>.
            
            <br><br>
            Directives in Vue start with the prefix <strong>v-</strong>, this lets you easily identify and know they are special attributes provided by Vue. Directives apply what it's called <strong>Reactive Behaviors</strong> to the DOM. For the case of <strong>v-bind</strong> directive that we saw in the previous slide, the behavior is basically Vue making sure the DOM element that contains the tag is being monitored for any changes and making sure the element stays updated automatically.
            <br><br>
            <blockquote>
            A directive’s job is to reactively apply side effects to the DOM when the value of its expression changes. - VueJS Docs
            </blockquote>
            <br>
            So, let's say we want to add a data attribute to our app that contains the URL to an online Image and we want that image to be rendered reactively by Vue just like the rest of the data we defined. Then We could do something like this.
            <br><br>
            Let's define a new data attribute inside our Vue's data object, it will be named <strong>image</strong> and will contain a string representing the URL to the image we want to render. like this:
            <br>
            <br>
            <img src="./img/slide_04_vbind01.jpg"></image>
            <br>
            <br>
            So now we need to tell Vue in our HTML to render that image, as we mentioned before we need to use the <strong>v-bind directive</strong>. This directive is used directly inside the HTML tag, like this:
            <br>
            <br>
            <img src="./img/slide_04_vbind02.jpg"></image>
            <br>
            <br>
            So, if we refresh our browser with the index.html file, we should see our updated example working now. The image is now also a reactive monitored data binding and any change to it will be automatically handled by Vue and the image will be re-rendered accordingly. This is how the full example looks now:
            <br>
            <br>
            <img src="./img/slide_04_vbind03.jpg"></image>
            <br>
            <br>
            <h2>Passing Arguments</h2>
            Vue provides some directives that can receive an argument, this is done by <strong>typing a colon :</strong> after the directive name. This means we can <strong>reactively</strong> update HTML attributes. If you noticed the example above, we already used this with success to reactively bind the URL of the image to the image element, more precisely to it's <strong>src</strong> HTML attribute. But we can also use what's called <strong>shorthands</strong> for the <strong>v-bind directive</strong> by putting a colon before the attribute name, like this:
            <br>
            <br>
            <img src="./img/slide_05_directiveArgs01.jpg"></image>
            <br>
            <h2>Dynamic Arguments? yep. </h2>
            With Vue version 2.6.0 and up, there is now the possibility to use JS expressions inside the directive arguments, as long as we make sure to <strong>wrap the expression between square brackets [ ]</strong>. So Vue can now, dynamically evaluate a JS expression to determine the final value for the binded attribute. Let see this with an example.

            <br><br>
            We first add a new data property (note this is not limited to data properties defined in our Vue's instance config object), that will hold the value of a given URL. Like this.
            <br>
            <br>
            <img src="./img/slide_05_directiveArgs02.jpg"></image>
            <br>
            Then we need to create a new HTML element that will consume that URL through an argument being passed down by a Vue JS directive binded to the HTML attribute of that element. WHAT? let's see it:
            <br>
            <br>
            <img src="./img/slide_05_directiveArgs03.jpg"></image>
            <br>
            So, we bind to the <strong>href attribute of a link (a) element</strong> the <strong>resulting value of the JS expression we named useThisURL</strong>. One important thing to mention is that <strong>dynamic arguments must always evaluate to a String</strong> with the sole exception of <strong>null</strong> which is reserved to be used to <strong>explicitly remove the binding</strong>. Any value different from a String will trigger warnings.
            <br><br>
            If we add these snippets of code we just saw into our previous example and refresh the browser, we can see that when the hover over the link element, we get the expected value displayed in the browser's status bar. Of course, clicking this link will open that same link.
            <br>
            <br>
            <img src="./img/slide_05_directiveArgs04.jpg"></image>
            <br>
            <h2>Handling events with directives</h2>
            Vue provides a lot of directives you can use in your app. Another one of the most popular directives is called the <strong>v-on</strong> directive, and it let us react to events in a simple manner. So, let's say we want to add to our example a button and we want to trigger an alert when it's clicked. Let's see how we can do this with v-on directive.
            <br><br>
            The first thing we need is to add something we mentioned but never used so far, a method. It'll be a really basic function that will show an alert. And we'll use that function as the argument we'll pass to the <strong>v-on directive</strong>. So, let's add a new method to our Vue instance in our <strong>app.js</strong> file.
            <br>
            <br>
            <img src="./img/slide_05_directiveEvents00.jpg"></image>
            <br>
            As we can see, the methods can be defined inside our Vue instance's config object. Just like we did with our data. So now let's see how we use that method in our <strong>index.html</strong> file and with our v-on directive.
            <br>
            <br>
            <img src="./img/slide_05_directiveEvents01.jpg"></image>
            <br>
            Quite easy right? but before we continue, notice that <strong>@click</strong>? that is basically this <strong>v-on:click="showAlert"</strong> which is telling Vue to trigger the <strong>showAlert</strong> method we created any time the event <strong>click</strong> is triggered on this element. So let's see what happens if we refresh our browser with our newly updated index.html and app.js files. I'll once again add into our example. Once loaded if we click on our newly added button, we'll get the expected alert.
            <br>
            <br>
            <img src="./img/slide_05_directiveEvents02.jpg"></image>
            <br>
            In the next slide we'll cover a couple more of directives that you'll find very useful when trying out Vue on your own.
            <blockquote>
                I've prepared these directives example for you to try in your browser, please <a href="https://scrimba.com/c/cmRyrvsM">Click here</a> and check it out.
            </blockquote>
            <br><br>
            Before we move on to components, I want to briefly tell you about a couple of directives that will make your life much easier when dealing with some common scenarios in our app.
            <div style="opacity:0.4; font-style: bold; font-size: 15px; text-align:center;">(When you are ready, advance to the next slide to continue)</div>

        `,
        image: ''
    },
    {
        id: 7,
        title: "Directives II",
        content: `
        <h2>Arrays and directives</h2>
        A common thing is that we have a series of data in an array structure and we'd like to have that data printed out for example as a list. This is a great use case for the <strong>v-for</strong> directive. This directive let us render a list of items without having to write one element for each data item present in our array. So, let's check this out. Let's say we have a list of other JS frameworks and libraries we want to print out without any specific order, as a list, in out app. <br><br>
        We start as usual by defining this new data in our Vue instance.
        <br>
        <br>
            <img src="./img/slide_06_directiveVfor01.jpg"></image>
        <br>
        Notice we defined an array of objects with our favorite JavaScript Frameworks and libraries. Now we need to render this list of items in out HTML. Let's add our list element and see how we use the new directive to handle the rendering for us.
        <br>
        <br>
            <img src="./img/slide_06_directiveVfor02.jpg"></image>
        <br>
        Notice how we specify <strong>v-for="tech in jsTechnologies"</strong>, being tech a data item of our array. This means that we can use string interpolation to render out any property of that data item <strong>tech</strong> such as the <strong>tech.name</strong> property. The rest is simple HTML we just tell Vue to handle the rendering of the <strong>li</strong> items inside the unordered list we created (ol). So Vue will iterate our array and render a list item for each data item we stored in such array. Pretty handy to deal with list quickly, right?

        <h2>What about user input? </h2>
        Another common scenario is when you need to handle user entering some data and you then have to do something with it, like (for the sake of simplicity) render that data out to the page right away. So far, we covered one-way data bindings, meaning that we would let Vue monitor some data we defined that was linked to a given HTML element, and Vue will reactively render that data upon any change to it happen. But what happens if we want to bind data two-ways? Let's say we want to have an input that receives any user data and we want to display below that input element the same text as soon as it is being typed. Let's check it out.
        <br>
        <br>
        We create a new data attribute in our Vue instance, in our <strong>app.js</strong> file.
        <br>
        <br>
            <img src="./img/slide_06_directiveVmodel01.jpg"></image>
        <br>
        Then we add a few new elements to showcase the new directive. An <strong>input</strong> element and a <strong>p</strong> paragraph element. The first will be used to two-way bind the data the user inputs to the second element, the paragraph. And Vue will be reactively updating the paragraph with whatever the user types into that input. So, our new HTML looks like this:
        <br>
        <br>
            <img src="./img/slide_06_directiveVmodel02.jpg"></image>
        <br>
        We defined our HTML elements and we also use string interpolation as before, but this time to let Vue update the value of our paragraph automatically. If we update our example and run it, we'll see the results.
        <br>
        <br>
            <img src="./img/slide_06_directiveVmodel03.gif"></image>
        <br>
        <blockquote>
                To try these directives examples in your browser, please <a href="https://scrimba.com/c/cmRyrvsM">Click here</a>.
            </blockquote>
        <br>
        That's it, pretty exciting framework so far. Right? If you consider how much more code would have been needed to do the same things with vanilla JS, that's already a win. But also, how fast and easy is to learn the basics of how to  use start using Vue. Then you definitely start to understand why this framework is rapidly becoming a widely used tool form simple apps to large scale solutions. Even for rapid FE prototyping it feels quick and simple. And we just started to scratch just the surface of what Vue can do. As for directives, I encourage you to go through the official VueJS documentation <a href="https://vuejs.org/v2/api/">here</a> to learn more about directives as they are a key piece of developing with VueJS and they are a lot more to study that we just can't cover here.
        <br>
        <br>
        <div style="opacity:0.4; font-style: bold; font-size: 15px; text-align:center;">(When you are ready, advance to the next slide to continue)</div>
        `,
        image: ''
    },
    {
        id: 8,
        title: "Components",
        content: `Components are reusable Vue instances with a specific name that we can use inside the scope of our main Vue instance defined for our app. This means that using components we can construct our app from small maintainable pieces of code, which are also reusable. Components have also support to the same options we can define in our main Vue instance. This means we can have a component with its own <strong>data, methods, etc.</strong> just like we've seen so far for our main instance.
        <br>
        <br>
        Components can also be reused as much as we need, and this can be done by just using the component's tag as needed. And they have their own HTML template that will be rendered by Vue each time it gets used. Let's see components with an example. <br><br>

        At the top of our app.js file we can start defining our component by using the <strong>Vue</strong> function as we did for our main instance. But for components before passing the configuration object we also need to provide a name for the component. Like this:
        <br>
        <br>
            <img src="./img/slide_07_components01.jpg"></image>
        <br>
        Notice we are also defining a template for the component; this is an HTML structure that will be rendered by Vue whenever we use our component. In this case the HTML is contained between <strong>backticks \` \`</strong> and consist of a simple <strong>h2</strong> element. And we set the value of that h2 to the <strong>learningTopic</strong> data property we are defined in the data function. Notice something new here? the data property in our component is not a on object like we did in our main Vue Instance. Instead we are defining it as a function. But why? The reason for that is so that each instance of this component can maintain an independent copy of the returned data.
        <br>
        <br>
        Then we update our <strong>index.html</strong> file to use this component. To use our component, we just need to enclose the component name we provided between tag notation like this:
        <br>
        <br>
            <img src="./img/slide_07_components02.jpg"></image>
        <br>
        And also, as you can see, you can reuse the component as many times as you want by just repeating the tag as needed. This renders the following to the browser:
        <br>
        <br>
            <img src="./img/slide_07_components03.jpg"></image>
        <br>
        As you can see by just this super simple example, components are a great way of building the UI of our app by small, controlled blocks of code. Pretty neat.
        <br>
        <br>
        <div style="opacity:0.4; font-style: bold; font-size: 15px; text-align:center;">(When you are ready, advance to the next slide to continue)</div>
        `,
        image: ''
    },
    {
        id: 9,
        title: "Lifecycle Hooks",
        content: `Every and each component and main instance we create in Vue, go through a series of steps when created. This includes setting the data monitoring, rendering of the templates, mounting the instances into the DOM and keeping that DOM updated upon changes. During each of these steps, some specific functions called <strong>lifecycle hooks</strong> are executed. This lets us run our own code at each step.
        <br>
        <br>
        For example, for this presentation app you are using (that I created with Vue), I used one of those hooks called <strong>mounted</strong> which is called once the instance has been mounted into the DOM. Each hook works slightly different so for this one, the mounted step does not guarantee that all the child components that our component main contain, were also mounted. So to make sure your code runs only when this happens, we can use <strong>this.$nextTick()</strong> function like this. <strong>Note this code is not part of the examples we've been doing during these slides, but a part of the code that makes this slides app work. </strong> Let me show you that bit of code.
        <br>
        <br>
            <img src="./img/slide_08_hooks01.jpg"></image>
        <br>
        I use this code to control the functions that get triggered each time you press one of the hot-keys I configured so you can navigate through this presentation with ease (f, spacebar, left-arrow, right-arrow). Notice also I used the <strong>this.$nextTick()</strong> function just to be sure the code gets processed once all the components were mounted. <br><br>
        There are several other hooks you can use; I encourage you to read about them <a href="https://vuejs.org/v2/api/#Options-Lifecycle-Hooks">here in the VueJS docs</a>.
        <br>
        <h2>Lifecycle Diagram</h2>
        Vue documentation provides this useful diagram to get a look at the overall lifecycle hooks and when they enter into picture during the execution time.
        <br>
        <br>
            <img class="diagram" src="./img/slide_08_lifecycle.png"></image>
        <br>
        <br>
        <br>
        <div style="opacity:0.4; font-style: bold; font-size: 15px; text-align:center;">(When you are ready, advance to the next slide to continue)</div>
        `,
        image: ''
    },
    {
        id: 10,
        title: "The end",
        content: `That's it for this introduction to VueJS! I hope you've found the content useful and that you can take advantage of the power and ease offered by Vue to develop your projects. You now have the basics to start building apps with Vue and to move on and learn more advanced Vue features.
        <br><br>
        I did certainly have a great time learning about VueJS to build this little presentation and the content that goes along with it. Below I leave to you a few links to the repository of this presentation and some good readings about Vue. Thank you for taking the time to read through and best of luck.
        <br>
        <br>
        <br>
        <br>
        <div style="text-align:center;">
            <a href="https://gitlab.com/tzero86/datatypes/tree/master/project2" title="Open Repository on GitLab" style="text-decoration:none;">
                <i class="fab fa-gitlab button" style="font-size:40px; padding:20px;"></i>
            </a>

            <a href="https://vuejs.org/v2/api/" title="Open VueJS API Docs" style="text-decoration:none;">
                <i class="fab fa-vuejs button" style="font-size:40px; padding:20px;"></i>
            </a>

            <a href="https://www.vuemastery.com/courses/intro-to-vue-js/vue-instance" title="Vue Mastery Free Online Course" style="text-decoration:none;">
                <i class="fas fa-hat-wizard button" style="font-size:40px; padding:20px;"></i>
            </a>

            <a href="https://www.udemy.com/vuejs-2-the-complete-guide/" title="Maximilian Schwarzmüller's Vue Course" style="text-decoration:none;">
                <i class="fas fa-user-tie button" style="font-size:40px; padding:20px;"></i>
            </a>

            <a href="https://github.com/tzero86" title="Check out my profile on GitHub" style="text-decoration:none;">
                <i class="fab fa-github button" style="font-size:40px; padding:20px;"></i>
            </a>
            <br><br>
            <br><br>
            <div style="opacity:0.4; font-style: bold; font-size: 15px; text-align:center;">This is the end of the presentation. But feel free to go back and review any of the slides. </div>
        </div>
        `,
        image: ''
    },
]

