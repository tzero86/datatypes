/*
    by @tzero86
 */

Vue.component("slide-item", {
    props: ['title', 'content', 'image', 'video'],
    template: `
        <div>
            <h1 class="title container">{{title}}</h1>
            <div class="content">
                <p id="slideContent" v-html="content"></p>
                <img v-if="image" :src="image" class="content-image"></img>
            </div>
        </div>`
});

Vue.component("controls", {
    props: ['prev', 'next', 'toggle'],
    template: `
    <div class="controls">
        <a id="prev" href="#" v-on:click="prev" class="button"  title="Previous Slide (Left-Arrow Key)">
            <i class="fas fa-chevron-circle-left"></i>
        </a>
        <a id="full" href="#" v-on:click="toggle" class="button"  title="Toggle FullScreen Mode(F Key)">
            <i class="fas fa-chevron-circle-up"></i>
        </a>
        <a id="next" href="#" v-on:click="next" class="button" title="Next Slide (Right-Arrow key)">
            <i class="fas fa-chevron-circle-right"></i>
        </a>
    </div>`
});

Vue.component("slide-counter", {
    props:['current', 'total', 'incfont', 'decfont'],
    template: `
    <div class="counter">
        <p> <a href="#" @click="decfont" title="Reduce Font Size" style="font-size:14px"> A-</a> <a href="#" title="Increase Font Size" @click="incfont">A+</a> | ({{current+1}}/{{total}}) </p>
    </div>`
});

const app = new Vue({
    el: '#app',
    data: {
        slidePosition: 0,
        slides: slidesData,
        totalSlides: slidesData.length,
        fontSize: "12px"
    },
    mounted: function () {
        this.$nextTick(function () {
            // Code that will run only after the
            // entire view has been rendered
            document.onkeydown = function (e) {
                switch (e.key) {
                    case 'f':
                        // up arrow
                        app.toggleFullScreen();
                        break;
                    case " ":
                        // space bar
                        app.nextSlide();
                        break;
                    case 'ArrowLeft':
                        // left arrow
                        app.prevSlide();
                        break;
                    case 'ArrowRight':
                        // right arrow
                        app.nextSlide();
                    case '+':
                        app.increaseFont();
                        break;
                    case '-':
                        app.decreaseFont();
                        break;
                    default:
                        break;
                }
            };
        });
    },
    methods: {
        toggleFullScreen() {
            let nextButton = document.querySelector("#full");
            if (!document.fullscreenElement) {
                document.documentElement.requestFullscreen();
                nextButton.innerHTML = '<i class="fas fa-chevron-circle-down"></i>';
            } else {
                document.exitFullscreen();
                nextButton.innerHTML = '<i class="fas fa-chevron-circle-up"></i>';
            }
        },
        prevSlide() {
            if (this.slidePosition > 1) {
                this.slidePosition--;
                return;
            } else if (this.slidePosition - 1 === -1) {
                this.slidePosition = this.slides.length - 1;
            } else {
                this.slidePosition = 0;
                return;
            }
        },
        nextSlide() {
            let top = this.slidePosition + 1;
            if (this.slides.length === top) {
                this.slidePosition = 0;
                return;
            }
            this.slidePosition++;
            return;
        },
        increaseFont() {
            $('#slideContent').animate({'font-size': '+=2'});
            //this.resizeText2(10);
        },
        decreaseFont() {
            $('#slideContent').animate({'font-size': '-=2'});
            //this.resizeText2(-10);
        }
    }
});