/*
Pirple's ES6 Track, Homework Assignment #13: Classes

Tzero86

Details:

Create a class called "Vehicle" and add methods that allow you to set the "Make", "Model", "Year,", and "Weight".
The class should also contain a "NeedsMaintenance" boolean that defaults to False, and and "TripsSinceMaintenance" Integer that defaults to 0.
Next create a subclass that inherits the properties of Vehicle class. Call this new subclass "Cars".
The Cars class should contain a method called "Drive" that sets the state of a boolean isDriving to True.  It should have another method called "Stop" that sets the value of isDriving to false.
Switching isDriving from true to false should increment the "TripsSinceMaintenance" counter. And when TripsSinceMaintenance exceeds 100, then the NeedsMaintenance boolean should be set to true.
Add a "Repair" method to either class that resets the TripsSinceMaintenance to zero, and NeedsMaintenance to false.
Create 3 different cars, using your Cars class, and drive them all a different number of times. Then print out their values for Make, Model, Year, Weight, NeedsMaintenance, and TripsSinceMaintenance
*/


class Vehicle {

	constructor(Make, Model, Year, Weight) {
		this.Make = Make;
		this.Model = Model;
		this.Year = Year;
		this.Weight = Weight;
		this.NeedsMaintenance = false;
		this.TripsSinceMaintenance = 0;
	}
	repair() {
		this.NeedsMaintenance = false;
		this.TripsSinceMaintenance = 0;
	}

}

class Car extends Vehicle {

	constructor(Make, Model, Year, Weight) {
		super(Make, Model, Year, Weight);
		this.isDriving = false;
	}
	Drive(times = 1) {
		this.isDriving = true;
		while (this.TripsSinceMaintenance < times) {
			this.TripsSinceMaintenance++;
		}
		if (this.TripsSinceMaintenance > 100) {
			this.NeedsMaintenance = true;
		}
	}
	Stop() {
		this.isDriving = false;
	}

}

// we create the cars
const myCar1 = new Car("Ford", "T", 1948, 1945); // let's say weights are in kilos.
const myCar2 = new Car("GMC", "OP Truck", 2008, 21670);
const myCar3 = new Car("BMW", "Z11", 2018, 4000);


// We drive the car1 100+ times and then stop it
myCar1.Drive(110);
myCar1.Stop();

// we drive the car 2 just 40 times and then stop it
myCar2.Drive(40);
myCar2.Stop();

// we drive our car3 just the default times(1) and then we stop it
myCar3.Drive();
myCar3.Stop();

// Now We print the requested information
console.log("myCar1 Details:");
console.table(myCar1);
console.log("myCar2 Details:");
console.table(myCar2);
console.log("myCar3 Details:");
console.table(myCar3);



/*

Extra Credit:

Create a Planes class that is also a subclass of Vehicle. Add methods to the Planes class for Flying and Landing (similar to Driving and Stopping), but different in one respect: Once the NeedsMaintenance boolean gets set to true, any attempt at flight should be rejected (return false), and an error message should be printed saying that the plane can't fly until it's repaired.

*/

class Plane extends Vehicle {
	constructor(Make, Model, Year, Weight) {
		super(Make, Model, Year, Weight);
		this.isFlying = false;
		this.canFly = true;
	}

	Flying(times) {
		this.isFlying = true;
		while (this.TripsSinceMaintenance < times && this.canFly) {
			this.TripsSinceMaintenance++;
		}
		if (this.TripsSinceMaintenance > 100) {
			this.NeedsMaintenance = true;
			this.canFly = false;
		}
	}
	Landing() {
		this.isFlying = false;
	}
}

// we Create two planes
const myF22 = new Plane("Lockheed Martin", "F-22 Raptor", 1997, 29000);
const myF35 = new Plane("Lockheed Martin", "F-35 Lightning II", 2006, 22426.06);

// we take our planes for a flight

myF22.Flying(300);
myF22.Landing();

myF35.Flying(100);
myF35.Landing();

console.log("myF22 Details:");
console.table(myF22);
console.log("myF35 Details:");
console.table(myF35);
console.log();