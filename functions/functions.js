/*
    All men are mortal
    Socrates is a man.
    Therefore, socrates is mortal.
*/

// we create the function using function expression and we also give a default argument value
const isMortal = function(personName = 'Socrates') {
    // we define an array of some famous mortal men
    let men = {
        menNames: ['Plato', 'Socrates', 'Pericles', 'Pythagoras' ],
        mortal: true,
    }
    // if men.mortal === true AND the name is part of the array of names
    if (men.mortal && men.menNames.includes(personName)) {
        // the person is mortal
        console.log(`${personName} is mortal.`);
        return true;
    } else { 
        // if any errors
        console.log(`Either ${personName} is not a man or Not all men are mortal.`);
        return false;
    }
}

// A valid Name
console.log(isMortal("Pericles"));
// A Name that is not part of known men
console.log(isMortal('Osiris'));
// We call it without parameters so it uses the default value 'Socrates'
console.log(isMortal());
// we call it with not a string to see if the conditional is working fine (if)
console.log(isMortal(13));

console.log("\n");

/* 
    This cake is either vanilla or chocolate.
    This cake is not chocolate.
    Therefore, this cake is vanilla.
*/

// we define the valid cake flavors
const cakeFlavors = ['Vanilla', 'Chocolate'];

/* 
    we create a function that returns the flavor of the cake
    if either of the arguments are undefined, defaults are used.
*/
 const whatsTheFlavor = ( flavors = cakeFlavors, isChocolate = false ) => {
    if (isChocolate) {
        // the cake is Chocolate
        return flavors[1];
    } else {
        // the cake is vanilla
        return flavors[0];
    }
};

// first case the cake is Chocolate.
console.log(whatsTheFlavor(cakeFlavors, true));

// second case the cake is NOT chocolate.
console.log(whatsTheFlavor(cakeFlavors, false));

// third case we just call the function without argument to test the default values
console.log(whatsTheFlavor());