/* 
    Pirple's Keeping up with the JavaScripts
    Homework #5: Switch Statements
*/

// we define the valid labels we'll be using.
const validLabels = ["second", "minute", "hour", "day", "seconds", "minutes", "hours", "days"];

// we check if the value is positive and the label is a valid one, but also their types
const checkIfLabelAndValueAreValid = (value, label) => {
    if (typeof value === "number" && typeof label === "string") {
        if (validLabels.includes(label)) {
            if (value >= 0) {
                // then we check if the combination of values is valid
                return checkIfCombOfValuesIsValid(value, label);
            } else {
                return false;
            }
        } 
    }
    return false;
};

// We check that the combination of values is correct
const checkIfCombOfValuesIsValid = (value, label) => {
    switch (label) {
        case validLabels[0]:
        case validLabels[1]:
        case validLabels[2]:
        case validLabels[3]:
            if (value === 1 || value === 0) {
                return true;
            } else {
                return false;
            }   
        case validLabels[4]:   
        case validLabels[5]: 
        case validLabels[6]:
        case validLabels[7]:
            if (value >= 2) {
                return true;
            } else {
                return false;
            }
        default:
            break;
    }
};

// Just a simple function to calculate the equivalent in seconds of the time passed.
const convertToSeconds = (value, label) => {
   switch (label) {
        case validLabels[0]: // second
            if (value === 0) {
                return 0;
            } else {
                return 1;
            }
        case validLabels[1]: // minute
            return 60;
        case validLabels[2]: // hour
            return 60*60;
        case validLabels[3]: // day
            return (24*60)*60;
        case validLabels[4]: // seconds
            return value;
        case validLabels[5]: // minutes
            return (value * 60);
        case validLabels[6]: // hours
            return (value * 60)*60;
        case validLabels[7]: // days
            return (value * 24)*60*60;
       default:
           break;
   } 
};


// we'll be returning our results in seconds so here we decide if we use singular/plural label
const selectLabel = (seconds) => {
    if (seconds > 1) {
        return validLabels[4];
    } else {
        return validLabels[0];
    }
};

// In this function I try to tackle the extra credit by converting seconds to day:hour:min:secs format. 
// Based on the result we pick the appropriate label
const formatForOutput = (seconds) => {
    let day = Math.floor(seconds / (3600*24));
    let hour = Math.floor(seconds % (3600*24) / 3600);
    let minute = Math.floor(seconds % 3600 / 60);
    let second = Math.floor(seconds % 3600 % 60);
    let dayResult = "";
    let hourResult = "";
    let minResult = "";
    let secResult = "";

    if (day > 0) {
        if (day === 1) { dayResult = `${day} ${validLabels[3]} `;
        } else { dayResult = `${day} ${validLabels[7]} `}
    } 
   
    if (hour > 0) {
        if (hour === 1) { hourResult = `${hour} ${validLabels[2]} `;
        } else { hourResult = `${hour} ${validLabels[6]} `;}
    } 
    
    if (minute > 0) {
        if (minute === 1) { minResult = `${minute} ${validLabels[1]} `;
        } else { minResult = `${minute} ${validLabels[5]} `;}
    } 
    
    if (second > 0) {
        if (second === 1) { secResult = `${second} ${validLabels[0]} `;
        } else { secResult = `${second} ${validLabels[4]} `;}
    }

    return dayResult + hourResult + minResult + secResult;
}

/*  Finally our function that consumes the logic of the previous functions
    we check if both pairs of values are valid if so we calculate the sum and return it as seconds.
    if either pair of values is not valid we return false.
*/ 
const timeAdder = (value1, label1, value2, label2) => {
    let result=  new Array(2);
    if ( checkIfLabelAndValueAreValid(value1, label1)  && checkIfLabelAndValueAreValid (value2, label2)) {
            result[0] = convertToSeconds(value1, label1) + convertToSeconds(value2, label2);
            result[1] = selectLabel(result[0]);
            console.log(result);
            console.log("Result Formatted for Output: " + formatForOutput(result[0]));
            return result;
    } else {
        console.log(false);
        return false;
    }
};


// some Test Cases
timeAdder(20,"hours",5,"hours");
timeAdder(1, "second", 0, "second");
timeAdder(1, "minute", 1, "second");
timeAdder(2, "days", 1, "minute");
timeAdder(1, "hour", 2, "minutes");
timeAdder(5, "hour", 5, "minutes");
timeAdder(false, false, 5, "minutes");
timeAdder({},"days",5,"minutes");
timeAdder(3,3,3,3);
timeAdder(1,"hour", 60,"minutes");
