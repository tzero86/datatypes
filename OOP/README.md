# What is OOP?
The idea behind OOP is to use Objects to model any real-world-things that we want to represent in our application. These Objects can contain data and functions, when those functions and data are stored inside the Object we say they are "encapsulated". We can also use inheritance to have Objects created based on our Original Object and the resulting object will contain all the properties defined in the master Object.

# Example:  Car Dealership App
For example Using OOP we can model an application that will handle the dealership of cars. Such application will have some main real-world representations as Automobile, Car, User and Customer. The application will offer a platform for users to post cars for sale and to purchase them.

## User Stories

- **New User is created** : A new user is registered on the app, when this happens the following taks are performed: A new customer (inherits from User Object) is created the newly registered user is taken to the dashboard where cars for sale are displayed.
- **User adds a car for sale**: A new instance of Car (inherits from Automobile Object) is created by a user. All car properties are set and the car is finally added to the list of cars for sale.
- **User searches for a car to buy**: The user gets a list of all available cars for purchase matching the search criteria entered.
- **User Buys a Car**: The car details are stored and liked to the user data by adding the car to the user.carsOwned property. The Car is then removed from the list of cars available and the list is refreshed.
- **User updates the details of a car**: The specific instance of "car" and its properties are updated with the new values. The data is refreshed where displayed.
- **User Deleted**: The selected userID is removed from the system. The user ID (if logged in) is disconnected. Any cars on sale published by the user are also removed from the cars-for-sale list.

## Objects
Using peseudocode we detail a bit our main Objects:
```js
Automobile -> Object { id, createdDate, brand, model, price }
User -> Object { id, Name, lastName, age, carsOwned }
```

And if we create a diagram(sort of) of these objects to see their relationships, we'll get:
![alt text](./Car-Users02.png "Simplified objects diagram")

From the diagram we can observe our two main Objects: User and Automobile. Then we see two other objects that inherit from User and Automobile called, Customer and Car. We notice also that we created a couple of cars and users and that one user owns the two cars.

## Pseudocode (or not so much)
Now we define some abstraction of code to see how this structure can be represented. This is done using JS syntax but we won't go into details with it, so we can say we are using JS as pesudocode here.

```js

// We create our User object
function User(id, name, lastName, age, ownedCars) {
    this.id = id;
    this.name = name;
    this.lastName = lastName;
    this.age = age;
    this.ownedCars = ownedCars;
}

// We create our Automobile object
function Automobile(id, createdDate, brand, model, price) {
    this.id = id;
    this.createdDate = createdDate;
    this.brand = brand;
    this.model = model;
    this.price = price;
}

// Our Car Object that inherits from Automobile
function Car(licenseNumb, hasDashCamera) {
    Automobile.call(this, id, createdDate, brand, model, price);
    this.licenseNumb = licenseNumb;
    this.hasDashCamera = hasDashCamera;
};

// Out customer Object that inherits from User
function Customer(isLoggedIn, userRating) {
    User.call(this, id, name, lastName, age, ownedCars);
    this.isLoggedIn = isLoggedIn;
    this.userRating = userRating;
};

// of course we can also add some methods to those objects.
Customer.prototype.sayName = function(){
    return `Hello, my name is ${this.name} and I'm ${this.age} years old.`
};

// same for the Car Object
Car.prototype.showLicensePlate = function (){
    console.log(this.licenseNumb);
}

// We create two new cars based on Automobile object.
const car1 = new Car(001, "04/12/19", "Ford", "T", 23489.78, "U45IJ78", false);
const car2 = new Car(002, "03/12/19", "GMC", "Roadster", 56855.554, "SIJ378A", true);

// For this simple demo we create an array to store the cars purchased by myNewUser below
let user1Cars = new Array();
user1Cars.push(car1);
user1Cars.push(car2);

// we create our new users
const myNewUser = new Customer(01, "Richard", "Grant", 32, user1Cars, true, 4.5);
const mySecondNewUser = new Customer(02, "Pete", "Dunn", 16, ["none"], false, 0.0);

// Our Pseudocode functionalities

function createUser() {
    // we create a new customer instance and store it in our DB.
    ...
}

function loadAvailableCars() {
    // We load all available cars for sale and render them out to the page.
    ...
}

function purchaseCar() {
    // When the customer purchases a car we add that car to the customer.ownedCars array
    // we also retire that car from the list of cars available to be purchased
    ...
}

function addCarForSale() {
    // We create a new instance of Car and assign the values.
    // Then we add that car to the list of cars available for sale.
    ...
}

function updateCar() {
    // receives a car object and lets the user update it's property values.
    // The car is also updated on the cars-for-sale list.
    ...
}

function deleteUser() {
    // we delete a given user from the records along with his published cars for sale
    // we also log the user out if he/she is logged in.
    ...
}

```

Since this is a high level abstraction we won't be defining any code for these functions.