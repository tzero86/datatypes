Hello,

This is my student repository for the course on ES6/Next "Keeping up with the JavaScripts" from Pirple.com

## List of Homework assignments

1. Homework #1: Data Types, see [Here](./dataTypes/song.js).
2. Homework #2: Variables, see [Here](./variables/variables.js).
3. Homework #3: Statements & Operators, see [Here](./statements%20and%20operators/socratesCakes.js).
4. Homework #4: Functions, see [Here](./functions/functions.js).
5. Homework #5: Switch Statement, see [Here](./switch/switch.js).
6. Homework #6: For Loops, see [Here](./forLoops/fizz.js).
7. Homework #7: The DOM, see [Here](./theDOM/index.html).
8. Homework #8: Events (Tic tac toe game), see [Here](./events/index.html).
9. Project #1: Todo List Management App(WriteCafe, updated for Homework #9), see [Here](./project1/index.html). [![Netlify Status](https://api.netlify.com/api/v1/badges/368821bf-3570-4731-8b7a-eae9cee7abaf/deploy-status)](https://app.netlify.com/sites/writecafe/deploys)
10. Homework #10: Destructuring Arrays and Objects, see [Here](./destructuring/destructuring.js).
11. Homework #11: Handling Exceptions [Here](./exceptions/exceptions.js).
12. Project #2: Introduction to Vue.js [Here](./project2/index.html). [![Netlify Status](https://api.netlify.com/api/v1/badges/f696dddd-dc0c-43cc-bfd5-3f3c2f07aa81/deploy-status)](https://app.netlify.com/sites/vuejsdemo/deploys)
13. Homework #12: Object Oriented Programming [Here](./OOP/README.md).
14. Homework #13: Classes [Here](./classes/app.js).


##

by tzero86.
