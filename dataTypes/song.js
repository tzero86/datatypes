/* 
    This is my attempt at resolving the data Types' homework number 1 from Pirple's course called
    keeping up with the javascripts.
    by tzero86
*/

// I must say, it is indeed 
let isPirpleAwesomeSoFar = true;
let yearsBeingPinkFloydFan= 20;

// An object with all the attributes I could identify from my favorite song.
let myFavSong = {
    songTitle: "The Thin Ice",
    album: "The Wall",
    genre: "Progressive rock, hard rock",
    yearOfRecording:  "April – November 1979",
    labels:"Harvest(UK), Columbia(US)",
    songwriters: "Roger Waters",
        
    // we use an array to store strings containing each song producer
    producers: ["Bob Ezrin", "David Gilmour", "James Guthrie", "Roger Waters"],
    releaseDate: "11/30/1979",
    durationInSeconds: 146,
    // another array containing some numbers with decimals representing specific minutes in the song m.ss (min, second, second)
    bestPartsAtMinute: [0.55, 1.22, 2.03],
    lyricsExtact: "You slip out of your depth and out of your mind, With your fear flowing out behind you. As you claw the thin ice.",
    
    // just a basic function to get the song producers nicely formatted.
    getSongProducers: function () {
    this.producers.forEach(function(producer) {
        console.log(producer);
        })
    }    
};

// Another object with the band information
const bandInformation = {
    // use of Strings
    artist: "Pink Floyd",
    formation: {
        bass: "Roger Waters",
        guitar: "David Gilmour",
        drums: "Nick Mason",
        piano: "Richard Wright",
    },
    // use of booleans
    isArtistStillActive: false,
    hallOfFameBand: true,
};

/* 
    We test some output and see if we get the expected information.
    I'm using string literals here to console log the information from the object properties plus the custom text and separators.
    This is to avoid using concatenation of strings mostly.
*/
console.log('>--------------------------------------------------------------------------------------------------<')
console.log(`My Favorite song is called ${myFavSong.songTitle}, It's from the album titled ${myFavSong.album}. Released in ${myFavSong.releaseDate}.`);
console.log(`Definitely a great song from ${bandInformation.artist}. I been a fan of them for about ${yearsBeingPinkFloydFan} years.`);
console.log(`This is my favorite passage of the lyrics "${myFavSong.lyricsExtact}"`);
console.log(`These are the song producers:`);

// we call the little function
myFavSong.getSongProducers();

// just a separator
console.log('>--------------------------------------------------------------------------------------------------<')

// Besides logging to the console some of the info of my fav song, I also print out the entire object structure with al the information declared.
console.log(`Anyways, this is all the data I got on this song and band: ${JSON.stringify(myFavSong,null,'\t') + JSON.stringify(bandInformation,null,'\t')}`);
