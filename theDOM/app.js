/*
    Homework #7
*/

// we define an array with some nice dark colors
const hexColors = [
  "#EAECEE",
  "#D5D8DC",
  "#ABB2B9",
  "#808B96",
  "#566573",
  "#2C3E50",
  "#273746",
  "#212F3D",
  "#1C2833",
  "#17202A"
];

// we create our main function that will create the elements and append them
const drawRectangles = () => {
  let appDiv = document.getElementById("rectangleWrapper");
  console.log("Here are all the rectangles IDs: \n");
  for (const index of hexColors) {
    const el = document.createElement("div");
    const colorText = document.createElement("p");
    el.id = `rect-${index}`;
    console.log(el.id);
    colorText.innerText = index;
    el.style.color = "white";
    el.style.backgroundColor = index;
    el.classList.add("rectangle");
    appDiv.appendChild(el);
    appDiv.appendChild(colorText);
  }
};

// this secondary function will take care of adding the title tag
const addTitle = () => {
  const title = document.createElement("title");
  const header = document.querySelector("#header");
  title.innerText = header.innerText;
  document.head.appendChild(title);
};

// this function will be used to call the others when the page loads
const start = () => {
  drawRectangles();
  addTitle();
};
