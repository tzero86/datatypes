/*
    Pirple's Homework Assignment #11: Exceptions
    By @tzero86

Details:
    For this assignment you're going to build a simple function and then reinforce it with some defensive code (to bullet-proof it against throwing exceptions).

The function should be called "reverseJsonArray" and it should accept one string,
and return either a string or false.

When called successfully, the function:
    1. Accepts one parameter: a string. A JSON-stringified version of a Javascript Array
    2. Parses the JSON to obtain the Array
    3. Reverses the order of the the items in the Array
    4. JSON-stringifies the result, and returns it.
*/


/*
    we test if the argument parsed as JSON is an array, if yes we stringify the result of a reversed
    version of the parsed argument and return that.
    - If the argument passed is not an array once parsed, we return false.
    - If any other errors the catch statement will also make sure the function returns false.
*/
const reverseJsonArray = (arr)=>{
    try {
        return Array.isArray(JSON.parse(arr)) ? JSON.stringify(JSON.parse(arr).reverse()) : false;
    } catch (error) { return false;}
};


// The Tests for our function

// 0. The assignment example
console.info(`Test #0: ${reverseJsonArray('["a","b","c"]')}`); // should return '["c","b","a"]'

// 1. Without any arguments
console.info(`Test #1: ${reverseJsonArray()}`); // should return false

// 2. With a boolean as the argument
console.info(`Test #2: ${reverseJsonArray(true)}`); // should return false

// 3. With an Array (non-stringified) as the argument
let nonStringyArr = ["a","b","c"];
console.info(`Test #3: ${reverseJsonArray(nonStringyArr)}`); // should return false

// 4. With a string argument that is not properly formatted JSON
console.info(`Test #4: ${reverseJsonArray("//notJSON//")}`); // should return false

// 5. With a stringified-array that only has one value
let singleValueArrr = ["a"];
console.info(`Test #5: ${reverseJsonArray(JSON.stringify(singleValueArrr))}`); // should return "[a]"

// 6. With a stringified-array that is empty
let emptyArr = [];
console.info(`Test #6: ${reverseJsonArray(JSON.stringify(emptyArr))}`); // should return "[]"

// 7. With a stringified-array that has an even-number of values
let stringyEvenArr = [1,2,3,4,5,6];
console.info(`Test #7: ${reverseJsonArray(JSON.stringify(stringyEvenArr))}`); // should return "[6,5,4,3,2,1]"

// 8. With a stringified-array that has an odd-number of values
let stringyOddArr = [1,2,3,4,5];
console.info(`Test #8: ${reverseJsonArray(JSON.stringify(stringyOddArr))}`); // should return "[5,4,3,2,1]"

// 9. With a undefined as parameter
console.info(`Test #9: ${reverseJsonArray(undefined)}`); // should return false

// 10. With null as parameter
console.info(`Test #10: ${reverseJsonArray(null)}`); // should return false

// 11. With some numbers
console.info(`Test #11: ${reverseJsonArray(123)}`); // should return false
